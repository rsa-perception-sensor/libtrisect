// clang-format off :
// This is copied verbatim from kernel source, don't reformat
#pragma once

#include <linux/videodev2.h>

// Custom user controls pulled from Allied Vision's custom kernel
// source:
//
// https://github.com/alliedvision/linux_nvidia_jetson/blob/master/kernel/avt/drivers/media/i2c/avt_csi2.h
//

#define V4L2_CID_EXPOSURE_AUTO_MIN (V4L2_CID_CAMERA_CLASS_BASE + 40)
#define V4L2_CID_EXPOSURE_AUTO_MAX (V4L2_CID_CAMERA_CLASS_BASE + 41)
#define V4L2_CID_GAIN_AUTO_MIN (V4L2_CID_CAMERA_CLASS_BASE + 42)
#define V4L2_CID_GAIN_AUTO_MAX (V4L2_CID_CAMERA_CLASS_BASE + 43)
#define V4L2_CID_EXPOSURE_ACTIVE_LINE_MODE (V4L2_CID_CAMERA_CLASS_BASE + 44)
#define V4L2_CID_EXPOSURE_ACTIVE_LINE_SELECTOR (V4L2_CID_CAMERA_CLASS_BASE + 45)
#define V4L2_CID_EXPOSURE_ACTIVE_INVERT (V4L2_CID_CAMERA_CLASS_BASE + 46)

/* Trigger mode to ON/OFF */
#define V4L2_CID_TRIGGER_MODE (V4L2_CID_CAMERA_CLASS_BASE + 47)

/* trigger activation: edge_rising, edge_falling, edge_any, level_high,
 * level_low */
#define V4L2_CID_TRIGGER_ACTIVATION (V4L2_CID_CAMERA_CLASS_BASE + 48)

/* trigger source: software, gpio0, gpio1 */
#define V4L2_CID_TRIGGER_SOURCE (V4L2_CID_CAMERA_CLASS_BASE + 49)

/* Execute a software trigger */
#define V4L2_CID_TRIGGER_SOFTWARE (V4L2_CID_CAMERA_CLASS_BASE + 50)

/* Camera temperature readout */
#define V4L2_CID_DEVICE_TEMPERATURE (V4L2_CID_CAMERA_CLASS_BASE + 51)

/* Binning mode: avg, sum */
#define V4L2_CID_BINNING_MODE (V4L2_CID_CAMERA_CLASS_BASE + 52)

#define AVT_CID_FIRMWARE_VERSION (V4L2_CID_CAMERA_CLASS_BASE + 54)
#define AVT_CID_CAMERA_NAME (V4L2_CID_CAMERA_CLASS_BASE + 55)
#define AVT_CID_SERIAL_NUMBER (V4L2_CID_CAMERA_CLASS_BASE + 56)

#define AVT_CID_ACQUISITION_STATUS (V4L2_CID_CAMERA_CLASS_BASE + 57)

#define AVT_CID_BINNING_SELECTOR (V4L2_CID_CAMERA_CLASS_BASE + 58)
