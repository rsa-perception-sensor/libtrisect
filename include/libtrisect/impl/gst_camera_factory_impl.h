// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <linux/videodev2.h>

#include <memory>
#include <sstream>

#if (GST_VERSION_MAJOR != 1)
#error "Only Gstreamer-1.0 supported"
#endif

namespace libtrisect {

// In truth, this is now IMX477+nvargusstreamer specific, rather than
// GStreamer-generic
//
template <typename T>
GstCamera::FactoryExpected GstCamera::CreateCamera(int width, int height,
                                                   int hz) {
  GstElement *pipeline;

  if (!gst_is_initialized()) {
    std::cerr << "Initializing gstreamer..." << std::endl;
    gst_init(0, 0);
  }

  std::cerr << "Gstreamer Version: " << gst_version_string() << std::endl;

  GError *error = 0;  // Assignment to zero is a gst requirement

  // \todo{amarburg} Better validation for image width/height
  // and framerate validity

  // \todo.  Need to validate that the incoming height/width are valid?

  // \todo(amarburg) Make this much more flexible in terms of setting
  // parameters, white balance, exposure time, etc
  //
  // clang-format off
  //
  // Some relevant parameters for nvarguscamerasrc (just a subset), retrieved
  // with "gst-inspect-1.0 nvarguscamerasrc"
  //
  // wbmode              : White balance affects the color temperature of the photo
  //                       flags: readable, writable
  //                       Enum "GstNvArgusCamWBMode" Default: 1, "auto"
  //                          (0): off              - GST_NVCAM_WB_MODE_OFF
  //                          (1): auto             - GST_NVCAM_WB_MODE_AUTO
  //                          (2): incandescent     - GST_NVCAM_WB_MODE_INCANDESCENT
  //                          (3): fluorescent      - GST_NVCAM_WB_MODE_FLUORESCENT
  //                          (4): warm-fluorescent - GST_NVCAM_WB_MODE_WARM_FLUORESCENT
  //                          (5): daylight         - GST_NVCAM_WB_MODE_DAYLIGHT
  //                          (6): cloudy-daylight  - GST_NVCAM_WB_MODE_CLOUDY_DAYLIGHT
  //                          (7): twilight         - GST_NVCAM_WB_MODE_TWILIGHT
  //                          (8): shade            - GST_NVCAM_WB_MODE_SHADE
  //                          (9): manual           - GST_NVCAM_WB_MODE_MANUAL
  // exposuretimerange   : Property to adjust exposure time range in nanoseconds
  //                       Use string with values of Exposure Time Range (low, high)
  //                       in that order, to set the property.
  //                       eg: exposuretimerange="34000 358733000"
  //                       flags: readable, writable
  //                       String. Default: null
  // gainrange           : Property to adjust gain range
  //                       Use string with values of Gain Time Range (low, high)
  //                       in that order, to set the property.
  //                       eg: gainrange="1 16"
  //                       flags: readable, writable
  //                       String. Default: null
  // ispdigitalgainrange : Property to adjust digital gain range
  //                       Use string with values of ISP Digital Gain Range (low, high)
  //                       in that order, to set the property.
  //                       eg: ispdigitalgainrange="1 8"
  //                       flags: readable, writable
  //                       String. Default: null
  // tnr-strength        : property to adjust temporal noise reduction strength
  //                       flags: readable, writable
  //                       Float. Range:              -1 -               1 Default:              -1
  // tnr-mode            : property to select temporal noise reduction mode
  //                       flags: readable, writable
  //                       Enum "GstNvArgusCamTNRMode" Default: 1, "NoiseReduction_Fast"
  //                          (0): NoiseReduction_Off - GST_NVCAM_NR_OFF
  //                          (1): NoiseReduction_Fast - GST_NVCAM_NR_FAST
  //                          (2): NoiseReduction_HighQuality - GST_NVCAM_NR_HIGHQUALITY
  // ee-mode             : property to select edge enhnacement mode
  //                       flags: readable, writable
  //                       Enum "GstNvArgusCamEEMode" Default: 1, "EdgeEnhancement_Fast"
  //                          (0): EdgeEnhancement_Off - GST_NVCAM_EE_OFF
  //                          (1): EdgeEnhancement_Fast - GST_NVCAM_EE_FAST
  //                          (2): EdgeEnhancement_HighQuality - GST_NVCAM_EE_HIGHQUALITY
  // ee-strength         : property to adjust edge enhancement strength
  //                       flags: readable, writable
  //                       Float. Range:              -1 -               1 Default:              -1
  // aeantibanding       : property to set the auto exposure antibanding mode
  //                       flags: readable, writable
  //                       Enum "GstNvArgusCamAeAntiBandingMode" Default: 1, "AeAntibandingMode_Auto"
  //                          (0): AeAntibandingMode_Off - GST_NVCAM_AEANTIBANDING_OFF
  //                          (1): AeAntibandingMode_Auto - GST_NVCAM_AEANTIBANDING_AUTO
  //                          (2): AeAntibandingMode_50HZ - GST_NVCAM_AEANTIBANDING_50HZ
  //                          (3): AeAntibandingMode_60HZ - GST_NVCAM_AEANTIBANDING_60HZ
  // exposurecompensation: property to adjust exposure compensation
  //                       flags: readable, writable
  //                       Float. Range:              -2 -               2 Default:               0
  //
  //
  // And with Jetpack 5.1.2 / Kernel 35.4.1
  //
  // GST_ARGUS: 3840 x 2160 FR = 29.999999 fps Duration = 33333334 ; Analog Gain range min 1.000000, max 22.250000; Exposure Range min 13000, max 683709000;
  // GST_ARGUS: 1920 x 1080 FR = 59.999999 fps Duration = 16666667 ; Analog Gain range min 1.000000, max 22.250000; Exposure Range min 13000, max 683709000;
  //
  // Despite message above, we can only set a gain range of 1-16
  std::ostringstream gsConfig;
  gsConfig << "nvarguscamerasrc sensor-id=0 gainrange=\"1 16\" wbmode=0 "
              "! capsfilter caps=video/x-raw(memory:NVMM),"
              "width=" << width << ",height=" << height <<
              ",format=(string)NV12,framerate=" << hz << "/1 ! nvvidconv flip-method=2 "
              " ! capsfilter caps=video/x-raw,format=BGRx";
  // clang-format on
  std::cerr << gsConfig.str() << std::endl;

  pipeline = gst_parse_launch(gsConfig.str().c_str(), &error);
  if (pipeline == NULL) {
    CameraFactoryError err;
    err.stream() << error->message;
    return tl::make_unexpected(err);
  }

  // Create RGB sink
  GstElement *sink = gst_element_factory_make("appsink", NULL);
  GstCaps *caps = gst_app_sink_get_caps(GST_APP_SINK(sink));

  caps =
      gst_caps_new_simple("video/x-raw", "format", G_TYPE_STRING, "BGRx", NULL);

  gst_app_sink_set_caps(GST_APP_SINK(sink), caps);
  gst_caps_unref(caps);

  // Set whether the sink should sync
  // Sometimes setting this to true can cause a large number of frames to be
  // dropped
  gst_base_sink_set_sync(GST_BASE_SINK(sink), FALSE);

  if (GST_IS_PIPELINE(pipeline)) {
    GstPad *outpad = gst_bin_find_unlinked_pad(GST_BIN(pipeline), GST_PAD_SRC);
    g_assert(outpad);

    GstElement *outelement = gst_pad_get_parent_element(outpad);
    g_assert(outelement);
    gst_object_unref(outpad);

    if (!gst_bin_add(GST_BIN(pipeline), sink)) {
      gst_object_unref(outelement);
      gst_object_unref(pipeline);

      CameraFactoryError err;
      err.stream() << "gst_bin_add() failed";
      return tl::make_unexpected(err);
    }

    if (!gst_element_link(outelement, sink)) {
      gst_object_unref(outelement);
      gst_object_unref(pipeline);

      CameraFactoryError err;
      err.stream() << "GStreamer: cannot link outelement(\""
                   << gst_element_get_name(outelement) << "\") -> sink";
      return tl::make_unexpected(err);
    }

    gst_object_unref(outelement);

  } else {
    GstElement *launchpipe = pipeline;
    pipeline = gst_pipeline_new(NULL);
    g_assert(pipeline);

    gst_object_unparent(GST_OBJECT(launchpipe));

    gst_bin_add_many(GST_BIN(pipeline), launchpipe, sink, NULL);

    if (!gst_element_link(launchpipe, sink)) {
      gst_object_unref(pipeline);

      CameraFactoryError err;
      err.stream() << "GStreamer: cannot link launchpipe -> sink";
      return tl::make_unexpected(err);
    }
  }

  // Use shared_ptr<T>( new T() ) as it can access procected construtors
  // make_shared cannot
  auto cam = std::shared_ptr<T>(
      new T(pipeline, sink, width, height, V4L2_PIX_FMT_XBGR32));
  return cam;
}

}  // namespace libtrisect
