// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

namespace libtrisect {

int xioctl(int fd, unsigned int request);
int xioctl(int fd, unsigned int request, void *arg);

#define CLEAR(x) memset(&(x), 0, sizeof(x))

// Helper function to convert fourcc into string
static std::string FourCCToString(__u32 nFourCC) {
  const char *pFourCC = (const char *)&nFourCC;
  std::ostringstream stream;
  stream << pFourCC[0] << pFourCC[1] << pFourCC[2] << pFourCC[3];

  return stream.str();
}

template <typename T>
V4LCamera::FactoryExpected V4LCamera::CreateCamera(const std::string &device,
                                                   int width, int height,
                                                   uint32_t pixformat,
                                                   GpioConfig trigger_gpio) {
  struct stat st;

  if (-1 == stat(device.c_str(), &st)) {
    CameraFactoryError err;
    err.stream() << "Cannot identify device " << device << "(" << errno
                 << "):" << strerror(errno);
    return tl::make_unexpected(err);
  }

  if (!S_ISCHR(st.st_mode)) {
    CameraFactoryError err;
    err.stream() << device << "is not a device ";
    return tl::make_unexpected(err);
  }

  int fd = open(device.c_str(), O_RDWR | O_NONBLOCK, 0);
  if (-1 == fd) {
    CameraFactoryError err;
    err.stream() << "Cannot open device " << device << "(" << errno
                 << "):" << strerror(errno);
    return tl::make_unexpected(err);
  }

  struct v4l2_capability cap;

  if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
    if (EINVAL == errno) {
      CameraFactoryError err;
      err.stream() << device << " is not a V4L2 device";
      close(fd);
      return tl::make_unexpected(err);
    } else {
      CameraFactoryError err;
      err.stream() << "Error querying capabilities for device " << device << "("
                   << errno << "):" << strerror(errno);
      close(fd);
      return tl::make_unexpected(err);
    }
  }

  if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
    CameraFactoryError err;
    err.stream() << device << " is not a video capture device";
    close(fd);
    return tl::make_unexpected(err);
  }

  if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
    CameraFactoryError err;
    err.stream() << device << " does not support streaming I/O";
    close(fd);

    return tl::make_unexpected(err);
  }

  struct v4l2_cropcap cropcap;
  CLEAR(cropcap);

  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
    struct v4l2_crop crop;
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c = cropcap.defrect; /* reset to default */

    if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop)) {
      switch (errno) {
        case EINVAL:
          /* Cropping not supported. */
          break;
        default:
          /* Errors ignored. */
          break;
      }
    }
  } else {
    /* Errors ignored. */
  }

  // Explicitly stop streaming
  {
    enum v4l2_buf_type type;
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type)) {
      CameraFactoryError err;
      err.stream() << "Error halting streaming for " << device << " (" << errno
                   << "):" << strerror(errno);
      close(fd);
      return tl::make_unexpected(err);
    }
  }

  // Free any existing buffers
  {
    struct v4l2_requestbuffers req;
    CLEAR(req);

    req.count = 0;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
      CameraFactoryError err;
      err.stream() << "Error freeing buffers for " << device << " (" << errno
                   << "):" << strerror(errno);
      close(fd);
      return tl::make_unexpected(err);
    }
  }

  {
    // Get the list of supported formats from the device
    std::cout << "Pixelformats supported by device (V4L2 FOURCCs):\n";
    for (__u32 i = 0;; i++) {
      v4l2_fmtdesc fmtdesc;
      memset(&fmtdesc, 0, sizeof(fmtdesc));
      fmtdesc.index = i;
      fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      if (-1 == xioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc)) {
        break;
      }

      // std::cout << i << " : " << FourCCToString(fmtdesc.pixelformat) << ", "
      //           << fmtdesc.description << std::endl;
    }
    std::cout << "\n";
  }

  struct v4l2_format fmt;
  CLEAR(fmt);
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  // Fill with current values
  xioctl(fd, VIDIOC_G_FMT, &fmt);

  if ((width > 0) && (height > 0)) {
    std::cerr << "Setting format for " << device << " to " << width << " x "
              << height << " with pixel_format 0x" << std::hex
              << static_cast<uint32_t>(pixformat) << std::dec << std::endl;

    fmt.fmt.pix.width = width;
    fmt.fmt.pix.height = height;
    fmt.fmt.pix.pixelformat = pixformat;
    fmt.fmt.pix.field = V4L2_FIELD_ANY;

    if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt)) {
      CameraFactoryError err;
      err.stream() << "Error setting format for device " << device << "("
                   << errno << "):" << strerror(errno);
      close(fd);
      return tl::make_unexpected(err);
    }
    /* Note VIDIOC_S_FMT may change width and height. */
  }

  /* Regardless of whether we try to set the video parameters, read them back.
   */
  if (-1 == xioctl(fd, VIDIOC_G_FMT, &fmt)) {
    CameraFactoryError err;
    err.stream() << "Error querying format for device " << device << "("
                 << errno << "):" << strerror(errno);
    close(fd);
    return tl::make_unexpected(err);
  }

  std::cerr << "Format for " << device << " was actually set to "
            << fmt.fmt.pix.width << " x " << fmt.fmt.pix.height
            << " with pixel_format 0x" << std::hex
            << static_cast<uint32_t>(fmt.fmt.pix.pixelformat) << std::dec
            << std::endl;

  /* Buggy driver paranoia. */
  unsigned int min = fmt.fmt.pix.width * 2;
  if (fmt.fmt.pix.bytesperline < min) fmt.fmt.pix.bytesperline = min;
  min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
  if (fmt.fmt.pix.sizeimage < min) fmt.fmt.pix.sizeimage = min;

  // Use shared_ptr<T>( new T() ) as it can access procected construtors
  // make_shared cannot
  auto cam = std::shared_ptr<T>(new T(device, fd, fmt.fmt.pix.width,
                                      fmt.fmt.pix.height,
                                      fmt.fmt.pix.pixelformat, trigger_gpio));
  std::cerr << "Successfully created camera for " << device
            << ", now initializing memory map" << std::endl;

  // Memory map initialization can also fail, so check it here
  auto ret = cam->initMemoryMap();
  if (ret < 0) {
    CameraFactoryError err;
    err.stream() << "Failed to initialize memory map (" << errno
                 << "):" << strerror(errno);
    // close(fd);  // will be closed in the destructor
    return tl::make_unexpected(err);
  }

  return cam;
}

}  // namespace libtrisect
