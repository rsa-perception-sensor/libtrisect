// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <assert.h>
#include <endian.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <memory>
#include <string>
#include <vector>

#include "libcsi_ioctl.h"
#include "libtrisect/camera.h"
#include "libtrisect/trigger/gpio.h"
#include "libtrisect/trigger/gpio_trigger.h"
#include "utils/expected.hpp"

namespace libtrisect {

class V4LCamera : public Camera {
 public:
  typedef std::shared_ptr<V4LCamera> SharedPtr;
  typedef tl::expected<V4LCamera::SharedPtr, CameraFactoryError>
      FactoryExpected;

  // Factory for creating a new camera instance.
  template <typename T = V4LCamera>
  static FactoryExpected CreateCamera(const std::string &device, int width = -1,
                                      int height = -1,
                                      uint32_t pixformat = V4L2_PIX_FMT_GREY,
                                      GpioConfig trigger_gpio = GpioConfig());

  //========

  virtual ~V4LCamera();

  std::string device() const { return _device; }

  std::shared_ptr<TriggerAction> configureTrigger(TriggerType t) override;
  virtual std::shared_ptr<TriggerAction> hardwareTriggerAction() {
    return std::make_shared<GpioTrigger>(trigger_gpio_);
  }

  std::shared_ptr<TriggerAction> softwareTriggerAction() {
    return std::make_shared<FunctionalTrigger>(
        [&]() { this->softwareTrigger(); });
  }

  // This is the same for both mono cameras right now
  virtual int enableHardwareTrigger(void) {
    enableV4LTrigger(V4L2_TRIGGER_SOURCE_LINE0,
                     V4L2_TRIGGER_ACTIVATION_RISING_EDGE);
    return 0;
  }

  int enableSoftwareTrigger(void) {
    enableV4LTrigger(V4L2_TRIGGER_SOURCE_SOFTWARE,
                     V4L2_TRIGGER_ACTIVATION_RISING_EDGE);
    return 0;
  }

  int disableHardwareTrigger(void);

  int softwareTrigger();

  // \todo The frameRate / fps API is still a little undercooked
  int setFrameRate(int numerator, int denominator);
  float fps(void);

  // typedef std::function< int() > GainFunction;
  // int setUserGainFunction(  GainFunction gainFunction );

  //=================================================================
  //
  // The following functions use the "V4L User Control" API

  v4l2_query_ext_ctrl queryV4LUserControl(uint32_t param);

  int setV4lUserControl(uint32_t param, int value);
  int setV4lUserControl64(uint32_t param, int64_t value);

  int getV4lUserControl(uint32_t param);
  int64_t getV4lUserControl64(uint32_t param);

  int getTriggerSource(void);
  int getTriggerActivation(void);

  int setFlip(bool horizontal, bool vertical);

  int setAutoGain(bool);

  virtual Camera::GainType gain(void);
  int setGain(Camera::GainType);

  int setAutoExposure(bool);

  virtual Camera::ExposureType exposure(void);
  int setExposure(Camera::ExposureType);

  template <typename T>
  MinMax<T> getV4LBounds(uint32_t id);

  typedef MinMax<Camera::ExposureType> ExposureMinMax;
  typedef MinMax<Camera::GainType> GainMinMax;

  ExposureMinMax exposureBounds(void);
  GainMinMax gainBounds(void);

  //=================================================================
  //
  // The following functions directly access Alvium registers
  // via the "generic" VIDEOC_R_I2C and VIDEOC_W_I2C ioctls
  //
  int getI2CRegister(uint32_t reg, void *buffer, size_t bytes);
  int setI2CRegister(uint32_t reg, void *buffer, size_t bytes);

  // These functions query the "absolute" I2C address
  template <typename T>
  T getI2CRegister(uint32_t reg) {
    T val = 0;
    getI2CRegister(reg, reinterpret_cast<void *>(&val), sizeof(T));
    return bytetohost(val);
  }

  // These functions query the "absolute" I2C address
  template <typename T>
  T setI2CRegister(uint32_t reg, T val_in) {
    T val = hosttobyte(val_in);
    setI2CRegister(reg, reinterpret_cast<void *>(&val), sizeof(T));
    return getI2CRegister<T>(reg);
  }

  template <typename T>
  T bytetohost(T in);
  // Include this to be pedantic, though it's the same as above
  template <typename T>
  T hosttobyte(T in) {
    return bytetohost<T>(in);
  }

  //  Reads 64-byte strings from Alvium registers
  std::string getI2CString(uint32_t reg);

  // And these query the V4L registers which are at a fixed offset in
  // the register map
  template <typename T>
  T getV4LRegister(uint32_t reg) {
    return getI2CRegister<T>(getV4L2RegisterOffset() + reg);
  }

  template <typename T>
  T setV4LRegister(uint32_t reg, T val) {
    return setI2CRegister<T>(getV4L2RegisterOffset() + reg, val);
  }

  uint16_t getV4L2RegisterOffset();

  std::string getDeviceGuid();
  std::string getManufacturerName();
  std::string getModelName();
  std::string getFamilyName();
  std::string getDeviceVersion();
  std::string getManufacturerInfo();
  std::string getSerialNumber();

  uint64_t getFirmwareVersion();
  uint8_t getCSI2LaneCount();

  uint32_t getMIPIDataFormat();
  uint64_t getAvailableMIPIDataFormats();

  uint32_t getV4LRegisterMapVersion();

  uint64_t getExposure();
  uint64_t getExposureMin();
  uint64_t getExposureMax();

  uint64_t getExposureAutoMin();
  uint64_t getExposureAutoMax();

  uint64_t getGain();
  uint64_t getGainMin();
  uint64_t getGainMax();

  uint64_t setGainRegister(uint64_t new_val);

  uint32_t getV4LHeight();
  uint32_t getV4LWidth();

  // Unlike the above, this function will scale the output to deg C
  uint32_t getTemperature();

 protected:
  // Private constructor, must use Camera::CreateCamera factory
  V4LCamera(const std::string &device, int fd, unsigned int width,
            unsigned int height, uint32_t pixformat, GpioConfig trigger_gpio);

  // Protected function which makes V4L calls to set up triggering
  int enableV4LTrigger(int trigger, int mode = -1);

 private:
  void clearBuffers();

  virtual void threadLoop(void);

  int fdioctl(unsigned int request);
  int fdioctl(unsigned int request, void *arg);

  int initMemoryMap(void);

  int startCapturing(void);
  int stopCapturing(void);

  int readFrame(void);

  std::string _device;
  int _fd;

  bool _doAutoGain, _doAutoExposure;

  GpioConfig trigger_gpio_;

  std::vector<struct Camera::Buffer_t> _buffers;
  bool cuda_zero_copy_;
};

/// \todo reduce DRY
template <typename T>
libtrisect::MinMax<T> V4LCamera::getV4LBounds(uint32_t param) {
  libtrisect::MinMax<T> output;
  struct v4l2_query_ext_ctrl queryctrl = queryV4LUserControl(param);

  if (queryctrl.id != param) {
    return output;
  }

  output._min = queryctrl.minimum;
  output._max = queryctrl.maximum;
  output._increment = queryctrl.step;

  std::cerr << "Param " << queryctrl.name << " min " << output._min << " max "
            << output._max << " step " << output._increment << std::endl;
  return output;
}

}  // namespace libtrisect

#include "libtrisect/impl/v4l_camera_factory_impl.h"
