// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <memory>
#include <string>

#include "libcsi_ioctl.h"
#include "libtrisect/gst_camera.h"
#include "libtrisect/trigger/gpio_trigger.h"
#include "libtrisect/v4l_camera.h"

namespace libtrisect {

using std::string;

class MonoCamera : public V4LCamera {
 public:
  friend class V4LCamera;

  enum MonoDataFormat_t {
    FORMAT_GREY8 = V4L2_PIX_FMT_GREY,
    FORMAT_GREY10 = 0x3059584A,  // FOURCC(JXY0)
    FORMAT_GREY12 = 0x3259584A   // FOURCC(JXY2)
  };

  static V4LCamera::FactoryExpected CreateCamera(
      const string &device, MonoDataFormat_t format,
      const GpioConfig trigger_gpio) {
    return V4LCamera::CreateCamera<MonoCamera>(
        device, 1920, 1216, static_cast<uint32_t>(format), trigger_gpio);
  }

 protected:
  MonoCamera(const string &device, int fd, int width, int height,
             uint32_t pixformat, const GpioConfig trigger_gpio)
      : V4LCamera(device, fd, width, height, pixformat, trigger_gpio) {}
};

class ColorCamera : public GstCamera {
 public:
  friend class GstCamera;

  // These resolutions are fixed by the kernel IMX477 driver
  //
  // The enum must match the sensor_mode from the sensor
  enum class Resolution { RES_3840_2160 = 0, RES_1920_1080 = 1, NUM_RES = 2 };

  // At present, camera frame rate is fixed on startup...
  static GstCamera::FactoryExpected CreateCamera(
      Resolution res = Resolution::RES_3840_2160, int hz = 10) {
    if (res == Resolution::RES_3840_2160) {
      return GstCamera::CreateCamera<ColorCamera>(3840, 2160, hz);
    } else if (res == Resolution::RES_1920_1080) {
      return GstCamera::CreateCamera<ColorCamera>(1920, 1080, hz);
    }

    return tl::make_unexpected(
        GstCamera::CameraFactoryError("Unknown resolution"));
  }

 protected:
  // Protected constructor is called from factory GstCamera::CreateCamera
  ColorCamera(GstElement *pipeline, GstElement *sink, int width, int height,
              int pixformat)
      : GstCamera(pipeline, sink, width, height, pixformat) {}
};

struct CamFactory {
  virtual V4LCamera::FactoryExpected LeftCamera(
      MonoCamera::MonoDataFormat_t format) = 0;
  virtual V4LCamera::FactoryExpected RightCamera(
      MonoCamera::MonoDataFormat_t format) = 0;
  virtual GstCamera::FactoryExpected ColorCamera(
      ColorCamera::Resolution resolution, int hz) = 0;
};

struct DrysectCameras : public CamFactory {
  inline static const string LeftCameraPath = "/dev/video1";
  inline static const string RightCameraPath = "/dev/video0";

  // Orin NX / J401 -specific trigger definitions
  // (not actually sure about the left-right mapping here)
  inline static const GpioConfig LeftTriggerGpio =
      GpioConfig({"/dev/gpiochip0", 138});
  inline static const GpioConfig RightTriggerGpio =
      GpioConfig({"/dev/gpiochip0", 49});

  V4LCamera::FactoryExpected LeftCamera(
      MonoCamera::MonoDataFormat_t format) override {
    return MonoCamera::CreateCamera(LeftCameraPath, format, LeftTriggerGpio);
  }

  V4LCamera::FactoryExpected RightCamera(
      MonoCamera::MonoDataFormat_t format) override {
    return MonoCamera::CreateCamera(RightCameraPath, format, RightTriggerGpio);
  }

  GstCamera::FactoryExpected ColorCamera(ColorCamera::Resolution resolution,
                                         int hz) override {
    Camera::CameraFactoryError err("No color camera on Drysect!");
    return tl::make_unexpected(err);
  }
};

struct TrisectCameras : public CamFactory {
  inline static const string LeftCameraPath = "/dev/video2";
  inline static const string RightCameraPath = "/dev/video1";
  inline static const string ColorCameraPath = "/dev/video0";

  // Xavier NX / Antmicro / Trisect-specific trigger definitions
  inline static const GpioConfig LeftTriggerGpio =
      GpioConfig({"/dev/gpiochip1", 96});
  inline static const GpioConfig RightTriggerGpio =
      GpioConfig({"/dev/gpiochip2", 15});

  V4LCamera::FactoryExpected LeftCamera(
      MonoCamera::MonoDataFormat_t format) override {
    return MonoCamera::CreateCamera(LeftCameraPath, format, LeftTriggerGpio);
  }

  V4LCamera::FactoryExpected RightCamera(
      MonoCamera::MonoDataFormat_t format) override {
    return MonoCamera::CreateCamera(RightCameraPath, format, RightTriggerGpio);
  }

  GstCamera::FactoryExpected ColorCamera(ColorCamera::Resolution resolution,
                                         int hz) override {
    return ColorCamera::CreateCamera(resolution, hz);
  }
};

}  // namespace libtrisect
