#pragma once

#include <condition_variable>
#include <list>
#include <mutex>

namespace libtrisect {

class MutexCondition {
 public:
  MutexCondition() {}

  void wait(void) {
    std::unique_lock<std::mutex> lk(_condMutex);
    _cond.wait(lk);
  }

  void notify_all() { _cond.notify_all(); }

  std::mutex _condMutex;
  std::condition_variable _cond;
};

}  // namespace libtrisect
