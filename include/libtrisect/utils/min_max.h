// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <cmath>
#include <iostream>

namespace libtrisect {

template <typename T>
struct MinMax {
  T _min, _max, _increment;

  MinMax() : _min(-1), _max(-1), _increment(-1) { ; }

  T fromScalar(int i) { return bound(_min + i * _increment); }
  int toScalar(T v) { return floor((bound(v) - _min) / _increment); }

  T bound(T v) {
    v = _min + _increment * std::round((v - _min) / _increment);
    return std::min<T>(_max, std::max<T>(_min, v));
  }

  bool valid() const { return (_min > 0) && (_max > 0) && (_increment > 0); }

  void setMax(T val) { _max = val; }
  void setMin(T val) { _min = val; }

  T max() const { return _max; }
  T min() const { return _min; }
};

}  // namespace libtrisect
