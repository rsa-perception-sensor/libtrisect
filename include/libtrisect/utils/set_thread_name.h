// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

namespace libtrisect {

// Utility fucntion for setting the process name of the current thread
void setThreadName(const char* threadName);

}  // namespace libtrisect
