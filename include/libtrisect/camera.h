// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <time.h>
#include <vpi/Image.h>

#include <functional>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include "libtrisect/trigger/trigger_service.h"
#include "libtrisect/utils/expected.hpp"
#include "libtrisect/utils/min_max.h"

namespace libtrisect {

class Camera {
 public:
  typedef std::shared_ptr<Camera> SharedPtr;
  typedef int64_t ExposureType;
  typedef int64_t GainType;

  //== Factory functions ==
  // Typedef for the factory return values
  struct CameraFactoryError {
    CameraFactoryError(int errnum, const std::string &msg = "")
        : err(errnum), _msgstr(msg) {}

    CameraFactoryError(const std::string &msg = "") : err(-1), _msgstr(msg) {}

    // Custom copy constructor
    CameraFactoryError(const CameraFactoryError &other)
        : err(other.err), _msgstr(other._msgstr.str()) {}

    std::stringstream &stream() { return _msgstr; }
    const std::string msg() const { return _msgstr.str(); }

    int err;
    std::stringstream _msgstr;
  };

  //========

  virtual ~Camera();

  void start(void);

  void setDone(void) { _done = true; }
  bool done(void) const { return _done; }

  void join(void) {
    if (_thread.joinable()) _thread.join();
  }

  unsigned int count() const { return _count; }
  int height() const { return _height; }
  int width() const { return _width; }

  int pixformat() const { return _v4l2format; }

  struct Buffer_t {
    // Pointers to the "raw memory"; these are used internally and are
    // not quaranteed to be set.
    uint8_t *userptr;
    size_t length;

    // VPIImage is a convenient wrapper for CPU/GPU buffers
    VPIImage image;

    // "time" is read from clock_gettime()
    struct timespec driver_timestamp;

    // timestamp from v4l (when relevant)
    struct timeval kernel_timestamp;
  };

  typedef std::function<void(const Buffer_t &)> NewImageCallback;
  void addImageCallback(NewImageCallback callback);

  void setFlash(char f) { _flash = f; }

  // Get the current exposure (in us)
  virtual ExposureType exposure(void) = 0;
  virtual GainType gain(void) = 0;

  virtual std::shared_ptr<TriggerAction> configureTrigger(TriggerType t) = 0;

 protected:
  // Private constructor, must use Camera::CreateCamera factory to
  // create new instances
  Camera(unsigned int width, unsigned int height, uint32_t pixformat);

  virtual void threadLoop(void) = 0;

  int _height, _width;
  uint32_t _v4l2format;

  std::vector<NewImageCallback> image_callbacks_;
  char _flash;

  unsigned int _count;

 private:
  bool _done;
  std::thread _thread;
};

}  //  namespace libtrisect
