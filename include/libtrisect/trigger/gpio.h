// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#pragma once

#include <linux/gpio.h>
#include <unistd.h>

namespace libtrisect {

struct GpioConfig {
  std::string device;
  int pin;

  bool valid(void) const {
    // \todo{} Could use more sophisticated test
    return device.size() > 0;
  }
};

class Gpio {
 public:
  Gpio(const GpioConfig &config);

  ~Gpio();

  void trigger();
  void untrigger();

 protected:
  bool configureGpio();

  // I'm not a huge fan of isOpen? status variables, should shift to factory?
  bool _is_open;

  std::string _gpioDevice;
  int _gpioFd;
  int _pin;

  struct gpiohandle_request _gpioHandle;
};

}  // namespace libtrisect
