// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <list>
#include <thread>

#include "libtrisect/trigger/trigger_service.h"
#include "libtrisect/utils/mutex_condition.h"

namespace libtrisect {

class TriggerLoop : public TriggerService {
 public:
  TriggerLoop();
  TriggerLoop(unsigned int period);

  ~TriggerLoop();

  bool setTimer();

  void start(void) override;
  bool setFps(float fps) override;
  void stop(void) override;

  void setDone(void) { _done = true; }
  bool done(void) const { return _done; }

  int count(void) const override { return _count; }

  void join(void) {
    if (_thread.joinable()) _thread.join();
  }

  void addTrigger(const std::shared_ptr<TriggerAction> &trigger) override;

 private:
  void threadLoop(void);

  unsigned int _period_ms;
  int _timer;
  std::mutex _timerMutex;

  int _count;

  std::thread _thread;
  bool _done;

  std::list<std::shared_ptr<TriggerAction> > _triggerList;

  MutexCondition cond_;

  TriggerLoop(const TriggerLoop &) = delete;
};

}  // namespace libtrisect
