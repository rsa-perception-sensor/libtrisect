// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <clocale>
#include <iostream>

#include "libtrisect/trigger/trigger_service.h"
#include "libtrisect/trigger/triggerd_constants.h"
#include "rpc.hpp"

namespace libtrisect {

class TriggerdClient : public TriggerService {
 public:
  TriggerdClient(const std::string &path = SocketPath);

  virtual void start(void) {}
  virtual void stop(void) {}

  virtual bool setFps(float fps) {
    set_period_ns(1e9 / fps);
    return true;
  }

  // Triggerd doesn't use triggerActions
  void addTrigger(const std::shared_ptr<TriggerAction> &trigger) override {}

  int32_t get_period_ns();
  int32_t set_period_ns(int32_t ns);

 protected:
  std::string _socket_path;
};

}  // namespace libtrisect
