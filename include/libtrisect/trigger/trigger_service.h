// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <memory>

#include "libtrisect/trigger/trigger_action.h"

namespace libtrisect {

enum class TriggerType { GPIO, TRIGGERD, SOFTWARE, DISABLED };

class TriggerService {
 public:
  TriggerService() {}

  virtual bool setFps(float fps) { return true; }

  virtual void start(void) {}
  virtual void stop(void) {}

  virtual void addTrigger(const std::shared_ptr<TriggerAction> &trigger) {}

  virtual int count() const { return 0; }
};

std::shared_ptr<TriggerService> MakeTriggerService(TriggerType type);

}  // namespace libtrisect
