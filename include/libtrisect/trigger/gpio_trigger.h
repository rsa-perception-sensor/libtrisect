// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#pragma once

#include "libtrisect/trigger/gpio.h"
#include "libtrisect/trigger/trigger_action.h"

namespace libtrisect {

class GpioTrigger : public TriggerAction {
 public:
  GpioTrigger(const GpioConfig &config) : TriggerAction(), _gpio(config) {}
  ~GpioTrigger() {}

 protected:
  void action() override {
    if (_flash) std::cerr << _flash;

    _gpio.trigger();
    // \todo This sets the "pulse width" for the trigger.
    // Should be configurable...
    usleep(1000);
    _gpio.untrigger();
  }

  Gpio _gpio;
};

}  // namespace libtrisect
