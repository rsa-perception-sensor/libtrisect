// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <functional>
#include <iostream>
#include <thread>

#include "libtrisect/utils/mutex_condition.h"

namespace libtrisect {

class TriggerAction {
 public:
  TriggerAction() : _thread(), _flash(0), _done(false), _count(0) { ; }

  void start(MutexCondition &cond) {
    _thread = std::thread(&TriggerAction::loop, this, std::ref(cond));
  }

  bool done() const { return _done; }
  void setDone() { _done = true; }

  void join() {
    if (_thread.joinable()) _thread.join();
  }

  int count() const { return _count; }

  void setFlash(char f) { _flash = f; }

 protected:
  // Inherited classes must replace _either_ loop _or_ action
  virtual void loop(MutexCondition &cond) {
    while (!done()) {
      cond.wait();
      action();
      ++_count;
    }
  }

  virtual void action() { ; }

  std::thread _thread;

  char _flash;

 private:
  bool _done;
  int _count;
};

class FakeTrigger : public TriggerAction {
 public:
  FakeTrigger(const std::string &msg) : TriggerAction(), _message(msg) { ; }

 protected:
  void action(void) override { std::cout << _message << std::endl; }

  std::string _message;
};

// class SoftwareTrigger : public TriggerAction {
// public:
//   SoftwareTrigger(const Camera::SharedPtr &camera) : TriggerAction(),
//   camera_(camera) { ; }

// protected:
//    void action(void) override { if(camera_) camera_->softwareTrigger(); }

//   Camera::SharedPtr camera_;
// };

class FunctionalTrigger : public TriggerAction {
 public:
  typedef std::function<void()> TriggerFunction;

  FunctionalTrigger(TriggerFunction f) : _action(f) {}

  void action() override {
    if (_action) _action();
  }

 protected:
  TriggerFunction _action;

  FunctionalTrigger() = delete;
  FunctionalTrigger(const FunctionalTrigger &) = delete;
};

}  // namespace libtrisect
