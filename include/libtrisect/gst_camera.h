/*
 * Draws heavily on [gscam](http://wiki.ros.org/gscam) which is also
 * released under a BSD license.
 */

#pragma once

#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>

extern "C" {
#include <gst/app/gstappsink.h>
#include <gst/gst.h>
}

#include "libtrisect/camera.h"
#include "libtrisect/utils/expected.hpp"

namespace libtrisect {

class GstCamera : public Camera {
 public:
  typedef std::shared_ptr<GstCamera> SharedPtr;

  typedef tl::expected<GstCamera::SharedPtr, CameraFactoryError>
      FactoryExpected;

  // Factory for creating a new camera instance.
  template <typename T = GstCamera>
  static FactoryExpected CreateCamera(int width, int height, int hz);

  //========

  virtual ~GstCamera();

  // \todo TBD implement later ... if it's even possible to get from gstreamer
  // inline
  virtual Camera::ExposureType exposure(void) { return -1; }
  virtual Camera::GainType gain(void) { return -1; }

  std::shared_ptr<TriggerAction> configureTrigger(TriggerType t) override {
    return nullptr;
  }

 protected:
  // Private constructor, must use GstCamera::CreateCamera factory
  GstCamera(GstElement *pipeline, GstElement *sink, unsigned int width,
            unsigned int height, int pixformat);

 private:
  int startStreaming();
  void stopStreaming();

  bool isPipelinePlaying();

  virtual void threadLoop(void);

  // Gstreamer structures
  GstElement *_pipeline;
  GstElement *_sink;

  // Appsink configuration
  bool sync_sink_;
  bool preroll_;
  bool reopen_on_eof_;
  bool use_gst_timestamps_;

  double time_offset_;

  // Camera publisher configuration
};

}  // namespace libtrisect

#include "libtrisect/impl/gst_camera_factory_impl.h"
