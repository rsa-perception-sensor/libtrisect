// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#pragma once

#include <condition_variable>
#include <list>
#include <mutex>
#include <thread>

#include "libtrisect/trigger/gpio_trigger.h"
#include "libtrisect/trigger/triggerd_constants.h"

namespace libtrisect {

class TriggerThread {
 public:
  static const int32_t TriggerOnTime_ns = 1e6;

  TriggerThread(bool is_drysect = false);
  TriggerThread(const TriggerThread&) = delete;

  ~TriggerThread();

  int32_t setPeriod(int32_t ns);
  int32_t getPeriod() const;

  int count(void) const { return _count; }

 private:
  void threadLoop(void);

  int _triggerTimer;
  int _untriggerTimer;
  mutable std::mutex _timerMutex;

  int _count;

  std::thread _thread;
  bool _done;

  struct Trigger {
    Gpio trigger;
    bool enabled;
  };

  std::array<Trigger, (uint)trigger_line_t::NUM_TRIGGER_LINES> _triggers;
};

}  // namespace libtrisect
