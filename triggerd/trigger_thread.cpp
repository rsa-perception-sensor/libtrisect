// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include "triggerd/trigger_thread.h"

#include <math.h>
#include <sys/timerfd.h>
#include <unistd.h>

#include <algorithm>
#include <iostream>

#include "libtrisect/trisect_cameras.h"

namespace libtrisect {

TriggerThread::TriggerThread(bool is_drysect)
    : _triggerTimer(timerfd_create(CLOCK_MONOTONIC, 0)),
      _untriggerTimer(timerfd_create(CLOCK_MONOTONIC, 0)),
      _count(0),
      _thread(),
      _done(false),
      // These values are known a priori from the Trisect hardware
      _triggers({{{is_drysect ? DrysectCameras::LeftTriggerGpio
                              : TrisectCameras::LeftTriggerGpio,
                   true},
                  {is_drysect ? DrysectCameras::RightTriggerGpio
                              : TrisectCameras::RightTriggerGpio,
                   true}}}) {
  setPeriod(0);
  _thread = std::thread(&TriggerThread::threadLoop, this);
}

TriggerThread::~TriggerThread() {
  _done = true;
  std::cerr << "Joining TriggerThread" << std::endl;
  if (_thread.joinable()) _thread.join();

  close(_triggerTimer);
  close(_untriggerTimer);
}

int32_t TriggerThread::setPeriod(int32_t ns) {
  std::cerr << "Configuring TriggerLoop with " << (ns / 1e6) << " ms delay"
            << std::endl;

  struct itimerspec itval;

  const int sec = floor(ns / 1e9);
  const int nsec = (ns - (sec * 1e9));
  itval.it_interval.tv_sec = sec;
  itval.it_interval.tv_nsec = nsec;
  itval.it_value.tv_sec = sec;
  itval.it_value.tv_nsec = nsec;
  {
    std::lock_guard<std::mutex> guard(_timerMutex);
    timerfd_settime(_triggerTimer, 0, &itval, NULL);
  }

  return getPeriod();
}

int32_t TriggerThread::getPeriod() const {
  struct itimerspec itval;
  // std::lock_guard<std::mutex> guard( _timerMutex );

  if (timerfd_gettime(_triggerTimer, &itval) == 0) {
    return itval.it_interval.tv_sec * 1e9 + itval.it_interval.tv_nsec;
  }

  return 0;
}

void TriggerThread::threadLoop() {
  fd_set rfds;
  int retval;

  std::cout << "Start trigger loop..." << std::endl;

  while (!_done) {
    FD_ZERO(&rfds);
    FD_SET(_triggerTimer, &rfds);
    FD_SET(_untriggerTimer, &rfds);

    {
      struct timeval timeout = {1, 0};
      retval = select(std::max(_triggerTimer, _untriggerTimer) + 1, &rfds, NULL,
                      NULL, &timeout);
    }

    if (retval > 0) {
      if (FD_ISSET(_triggerTimer, &rfds)) {
        // std::cout << "TimerLoop at " << time(NULL) << std::endl;

        for (auto &trigger : _triggers) {
          if (trigger.enabled) trigger.trigger.trigger();
        }
        ++_count;

        // Essential to read from the timer before select()ing again
        char buf[9];
        read(_triggerTimer, buf, 8);

        // Set untrigger timer
        struct itimerspec itval = {{0, 0}, {0, 0}};
        itval.it_value.tv_nsec = TriggerOnTime_ns;
        timerfd_settime(_untriggerTimer, 0, &itval, NULL);

      } else if (FD_ISSET(_untriggerTimer, &rfds)) {
        for (auto &trigger : _triggers) {
          trigger.trigger.untrigger();
        }

        char buf[9];
        read(_untriggerTimer, buf, 8);
      }

    } else if (retval == 0) {
      // Timeout, just loop again...
      // std::cout << "Timeout..." << std::endl;
    }
  }

  std::cout << "End trigger loop..." << std::endl;
}

}  // namespace libtrisect
