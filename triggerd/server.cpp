// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include <signal.h>
#include <sys/capability.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <atomic>
#include <boost/program_options.hpp>
#include <clocale>
#include <functional>
#include <iostream>
namespace po = boost::program_options;

#include "libtrisect/trigger/gpio_trigger.h"
#include "libtrisect/trigger/triggerd_constants.h"
#include "rpc.hpp"
#include "trigger_thread.h"

using libtrisect::triggerd_function_t;
using libtrisect::TriggerThread;

static void clear_signal_handlers() noexcept {
  signal(SIGINT, SIG_IGN);
  signal(SIGTERM, SIG_IGN);
}

static void install_signal_handlers(void (*handler)(int)) noexcept {
  signal(SIGINT, handler);
  signal(SIGTERM, handler);
}

static std::atomic<bool> g_stop = false;

static void signal_handler(int /*signum*/) noexcept {
  clear_signal_handlers();
  g_stop = true;
}

auto predicate = []() { return !g_stop; };

class TriggerdDispatcher {
 public:
  TriggerdDispatcher(bool is_drysect) : _loop(is_drysect) {}

  ~TriggerdDispatcher() {}

  void invoke(uint32_t id, ipc::in_message& in_msg, ipc::out_message& out_msg,
              ipc::point_to_point_socket& p2p_socket) const {
    switch ((triggerd_function_t)id) {
      case triggerd_function_t::set_period_ns:
        ipc::function_invoker<int32_t(int32_t), true>()(
            in_msg, out_msg,
            [this](int32_t ns) -> int32_t { return _loop.setPeriod(ns); });
        break;
      case triggerd_function_t::get_period_ns:
        ipc::function_invoker<int32_t(void), true>()(
            in_msg, out_msg, [this]() -> int32_t { return _loop.getPeriod(); });
        break;
      default:
        break;
    }
  }

  void report_error(const std::exception_ptr& p) const {
    if (!g_stop) {
      try {
        std::rethrow_exception(p);
      } catch (const std::exception& ex) {
        std::cout << "call error >> " << ex.what() << std::endl;
      }
    }
  }

  void ready() const { std::cout << "triggerd is ready" << std::endl; }

 protected:
  mutable TriggerThread _loop;
};

bool check_capabilities() {
  bool result = false;

  cap_t caps;
  caps = cap_get_proc();

  cap_flag_value_t value;

  if (cap_get_flag(caps, CAP_SYS_NICE, CAP_PERMITTED, &value) == 0) {
    std::cout << "CAP_SYS_NICE capability " << (value != CAP_SET ? "NOT " : "")
              << "set" << std::endl;

    result = (value == CAP_SET);
  } else {
    std::cerr << "Unable to query capability CAP_SYS_NICE" << std::endl;
  }

  cap_free(caps);

  return result;
}

int main(int argc, char** argv) {
  std::setlocale(LC_ALL, "");
  install_signal_handlers(signal_handler);

  bool is_drysect = false;

  try {
    // Parse command line options
    po::options_description options("Options");
    options.add_options()("drysect", po::bool_switch(&is_drysect),
                          "Use Drysect configuration")("help",
                                                       "produce help message");

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      return 1;
    }

  } catch (const po::error& ex) {
    std::cerr << ex.what() << '\n';
    exit(-1);
  }

  auto has_caps = check_capabilities();

  if (has_caps) {
    struct sched_param params;
    params.sched_priority = sched_get_priority_min(SCHED_FIFO);

    auto result = sched_setscheduler(0, SCHED_FIFO, &params);

    if (result < 0) {
      std::cerr << "Error changing scheduler: " << errno << std::endl;
    }

    if (sched_getscheduler(0) == SCHED_FIFO) {
      sched_getparam(0, &params);
      std::cerr << "   Scheduler is SCHED_FIFO, sched_priority="
                << params.sched_priority << std::endl;
    } else {
      std::cerr << "   Scheduler is not SCHED_FIFO as expected" << std::endl;
    }
  } else {
    std::cerr << "Daemon doesn't have CAP_SYS_NICE, run:" << std::endl;
    std::cerr << "     setcap cap_sys_nice+ep " << argv[0] << std::endl;
  }

  try {
    // Quick hack
    umask(0);

    if (is_drysect) {
      std::cerr << "Using __drysect__ GPIO" << std::endl;
    } else {
      std::cerr << "Using __trisect__ GPIO" << std::endl;
    }

    ipc::rpc_server<ipc::unix_server_socket> server(libtrisect::SocketPath);
    server.run(TriggerdDispatcher(is_drysect), predicate);
  } catch (const std::exception& ex) {
    if (!g_stop) {
      std::cout << "fatal error >> " << ex.what() << std::endl;
      return 1;
    }
  }

  return 0;
}
