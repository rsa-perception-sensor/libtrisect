#!/bin/bash

stream_left()
{
    v4l2-ctl -d /dev/video1 -p 100 --set-fmt-video=width=1920,height=1216,pixelformat=GREY --stream-mmap
}

stream_right()
{
    v4l2-ctl -d /dev/video0 -p 100 --set-fmt-video=width=1920,height=1216,pixelformat=GREY --stream-mmap
}

stream_color()
{
    v4l2-ctl -d /dev/video2 --set-fmt-video=width=1920,height=1080,pixelformat=RG10 --stream-mmap
}



case $1 in
    "left")
        stream_left
        ;;
    "right")
        stream_right
        ;;
    "color")
        stream_color
        ;;
    *)
        echo "usage:  $0 {left,right,color}"
        ;;
esac
