#!/bin/bash


# stream_left()
# {
#     v4l2-ctl -d /dev/video1 -p 100 --set-fmt-video=width=1920,height=1216,pixelformat=GREY --stream-mmap
# }

# stream_right()
# {
#     v4l2-ctl -d /dev/video0 -p 100 --set-fmt-video=width=1920,height=1216,pixelformat=GREY --stream-mmap
# }

stream_color()
{
    #
    # sensor-mode=-1 mean "select automatically"
    #
    # valid formats (determined experimentally): NV12
    GST_DEBUG=2 gst-launch-1.0 nvarguscamerasrc sensor-id=0 sensor-mode=-1 gainrange="1 16" \
                    ! capsfilter caps=video/x-raw\(memory:NVMM\),width=4032,height=3040,framerate=10/1 \
                    ! nvvidconv flip-method=2 \
                    ! capsfilter caps=video/x-raw,format=BGRx \
                    ! filesink location=foo.raw
}



case $1 in
    # "left")
    #     stream_left
    #     ;;
    # "right")
    #     stream_right
    #     ;;
    "color")
        stream_color
        ;;
    *)
        echo "usage:  $0 {color}"
        ;;
esac
