# libtrisect

Libtrisect is a set of userspace drivers for the [trisect]() three-camera underwater sensing system.   It contains:

* A (relatively) generic V4L video capture API wrapper.
* A (relatively) generic camera triggering system with support for `/dev/gpiochipX`
* Trisect-specific instances of camera and trigger devices with hardware-specific configuration compiled in.

Libtrisect will compile under ROS, or as a standalone library.  It contains no ROS-specific constructs, other than a `package.xml` and catkin configuration code in `CMakeLists.txt`.   [trisect_driver](https://gitlab.com/amarburg/trisect_driver) is full ROS driver which uses libtrisect.

The sample application [tools/RunCamera.cpp](tools/RunCamera.cpp) shows use of the libtrisect API.

# Why do we need a special hardware layer?

At a high level, the V4L code in libtrisect is based on the same [V4L capture example](https://www.kernel.org/doc/html/v4.9/media/uapi/v4l/capture.c.html) found elsewhere.  The combination of libtrisect+trisect_driver serves a similar function to ROS's [usb_cam](http://wiki.ros.org/usb_cam).

Libtrisect provides:

* Trisect-specific hardware configuration for triggering
* Camera configuration tuned for trisect
* The option for connecting sets of cameras in code, for example, having the exposure setting in one camera follow the auto-exposure setting in another.

# trisect_triggerd

This repo also contains a small program `triggerd` which implements a simple fixed rate GPIO triggering scheme, controlled over socket IPC.  This program is designed to be run by a privileged user such that it can give itself soft-realtime privileges (`CAP_SYS_NICE`).

[include/libtrisect/tiggerd_client.h](include/libtrisect/tiggerd_client.h) implements a simple client for communicating with `triggerd` through the socket.

`CMakeLists.txt` contains rules for building `triggerd` into a `.deb` package for system-wide installation.
In a CMake (non-catkin) build directory, run `make package` to build a `.deb` for triggerd.

# License

Libtrisect is released under the [BSD 3-clause license](LICENSE).  It contains multiple files released under licenses:

* [include/libcsi_ioctl.h](include/libcsi_ioctl.h) is set of constants from the Allied Vision NVidia Linux Kernel.
* [include/librisect/util/expected.hpp](include/librisect/util/expected.hpp) is copied from Sy Brand's [expected](https://github.com/TartanLlama/expected) package, which is released in the public domain.

It includes a fork of the [IPC](https://github.com/amarburg/IPC.git) repository as a Git submodule.
That library was released under the MPL by its original author.
