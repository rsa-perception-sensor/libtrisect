// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include "libtrisect/trigger/trigger_loop.h"

#include <sys/timerfd.h>
#include <unistd.h>

#include <cmath>
#include <iostream>

#include "libtrisect/trigger/trigger_action.h"

namespace libtrisect {

TriggerLoop::TriggerLoop()
    : TriggerService(),
      _period_ms(0),
      _timer(timerfd_create(CLOCK_MONOTONIC, 0)),
      _count(0),
      _thread(),
      _done(false),
      _triggerList(),
      cond_() {}

TriggerLoop::TriggerLoop(unsigned int period)
    : TriggerService(),
      _period_ms(period),
      _timer(timerfd_create(CLOCK_MONOTONIC, 0)),
      _count(0),
      _thread(),
      _done(false),
      _triggerList(),
      cond_() {}

TriggerLoop::~TriggerLoop() {
  setDone();
  join();

  close(_timer);
}

bool TriggerLoop::setTimer() {
  if (_period_ms == 0) return false;

  std::lock_guard<std::mutex> guard(_timerMutex);
  std::cerr << "Configuring TriggerLoop with " << _period_ms << " ms delay"
            << std::endl;

  struct itimerspec itval;

  int sec = _period_ms / 1000;
  int ns = (_period_ms - (sec * 1000)) * 1000000;
  itval.it_interval.tv_sec = sec;
  itval.it_interval.tv_nsec = ns;
  itval.it_value.tv_sec = sec;
  itval.it_value.tv_nsec = ns;
  timerfd_settime(_timer, 0, &itval, NULL);

  return true;
}

bool TriggerLoop::setFps(float fps) {
  int periodMs = round((1. / fps) * 1000);

  if ((periodMs > 0) && (periodMs < 1000)) {
    _period_ms = periodMs;
    return setTimer();
  }

  return false;
}

void TriggerLoop::addTrigger(const std::shared_ptr<TriggerAction> &trigger) {
  if (trigger) _triggerList.push_front(trigger);
}

void TriggerLoop::start(void) {
  _done = false;

  _thread = std::thread(&TriggerLoop::threadLoop, this);
}

void TriggerLoop::stop(void) {
  setDone();
  join();
}

void TriggerLoop::threadLoop() {
  // Launch all TriggerActions
  for (auto &trigger : _triggerList) {
    trigger->start(cond_);
  }

  setTimer();

  /* Wait */
  fd_set rfds;
  int retval;

  /* Watch timefd file descriptor */
  FD_ZERO(&rfds);
  FD_SET(0, &rfds);
  FD_SET(_timer, &rfds);

  while (!done()) {
    {
      std::lock_guard<std::mutex> guard(_timerMutex);
      retval = select(_timer + 1, &rfds, NULL, NULL, NULL);
    }

    if (retval > 0) {
      // std::cout << "TimerLoop at " << (int)time(NULL) << "(" <<
      // _triggerList.size() << ")" << std::endl;

      cond_.notify_all();
      ++_count;
    }

    // Essential to read from the timer before select()ing again?
    char buf[9];
    read(_timer, buf, 8);
  }

  // Reap all trigger threads
  for (auto &trigger : _triggerList) {
    trigger->setDone();
  }

  cond_.notify_all();

  for (auto &trigger : _triggerList) {
    trigger->join();
  }
}

}  // namespace libtrisect
