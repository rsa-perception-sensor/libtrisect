// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include "libtrisect/gst_camera.h"

#include <linux/videodev2.h>

#include "libtrisect/utils/set_thread_name.h"
#include "libtrisect/vpi_utils.h"

// Timespec conversion functions from https://github.com/solemnwarning/timespec

#define NSEC_PER_SEC 1000000000

static struct timespec timespec_normalise(struct timespec ts) {
  while (ts.tv_nsec >= NSEC_PER_SEC) {
    ++(ts.tv_sec);
    ts.tv_nsec -= NSEC_PER_SEC;
  }

  while (ts.tv_nsec <= -NSEC_PER_SEC) {
    --(ts.tv_sec);
    ts.tv_nsec += NSEC_PER_SEC;
  }

  if (ts.tv_nsec < 0) {
    /* Negative nanoseconds isn't valid according to POSIX.
     * Decrement tv_sec and roll tv_nsec over.
     */

    --(ts.tv_sec);
    ts.tv_nsec = (NSEC_PER_SEC + ts.tv_nsec);
  }

  return ts;
}

static struct timespec timespec_from_double(double s) {
  struct timespec ts = {
      static_cast<int32_t>(s),
      static_cast<int32_t>((s - (int32_t)(s)) * NSEC_PER_SEC)};

  return timespec_normalise(ts);
}

static double timespec_to_double(struct timespec ts) {
  return (static_cast<double>(ts.tv_sec) +
          (static_cast<double>(ts.tv_nsec) / NSEC_PER_SEC));
}

namespace libtrisect {

// Private constructor, must use GstCamera::CreateCamera factory
GstCamera::GstCamera(GstElement *pipeline, GstElement *sink, unsigned int width,
                     unsigned int height, int pixformat)
    : Camera(width, height, pixformat),
      _pipeline(pipeline),
      _sink(sink),
      preroll_(true) {}

GstCamera::~GstCamera() {
  if (isPipelinePlaying()) {
    stopStreaming();
  }

  if (_pipeline && GST_IS_ELEMENT(_pipeline)) {
    gst_element_set_state(_pipeline, GST_STATE_NULL);
    gst_object_unref(GST_OBJECT(_pipeline));
    _pipeline = NULL;
  }
}

// Cribbed from OpenCV:
//  modules/videoio/src/cap_gstreamer.cpp#L858
bool GstCamera::isPipelinePlaying() {
  if (!_pipeline || !GST_IS_ELEMENT(_pipeline)) {
    std::cerr << "GStreamer: pipeline have not been created" << std::endl;
    return false;
  }

  GstState current, pending;
  GstClockTime timeout = 5 * GST_SECOND;
  GstStateChangeReturn ret =
      gst_element_get_state(_pipeline, &current, &pending, timeout);
  if (!ret) {
    std::cerr << "unable to query pipeline state" << std::endl;
    ;
    return false;
  }

  return current == GST_STATE_PLAYING;
}

void GstCamera::threadLoop(void) {
  setThreadName("GstCamera::threadLoop");

  const int err = startStreaming();
  if (err < 0) {
    std::cerr << "Error starting capture: " << err << std::endl;
    return;
  }

  while (!done()) {
    // This should block until a new frame is awake, this way,
    // we'll run at the actual capture framerate of the device.
    GstSample *sample = gst_app_sink_pull_sample(GST_APP_SINK(_sink));

    if (!sample) {
      if (gst_app_sink_is_eos(GST_APP_SINK(_sink))) {
        std::cerr << "Could not get gstreamer sample: stream is EOS"
                  << std::endl;
      } else {
        std::cerr << "Could not get gstreamer sample." << std::endl;
      }
      break;
    }

    GstBuffer *buf = gst_sample_get_buffer(sample);

    if (!buf) {
      gst_sample_unref(sample);
      std::cerr << "Stream ended." << std::endl;
      setDone();
      break;
    }

    GstMemory *memory = gst_buffer_get_memory(buf, 0);
    GstMapInfo info;

    gst_memory_map(memory, &info, GST_MAP_READ);
    const gsize buf_size = info.size;
    guint8 *&buf_data = info.data;

    GstClockTime bt = gst_element_get_base_time(_pipeline);
    // ROS_INFO("New buffer: timestamp %.6f %lu %lu %.3f",
    //         GST_TIME_AS_USECONDS(buf->timestamp+bt)/1e6+time_offset_,
    //         buf->timestamp, bt, time_offset_);
    timespec frameTime;
    clock_gettime(CLOCK_REALTIME, &frameTime);

    // A null buffer indicates the stream has closed...

    int width, height;

    GstPad *pad = gst_element_get_static_pad(_sink, "sink");
    GstCaps *caps = gst_pad_get_current_caps(pad);
    GstStructure *structure = gst_caps_get_structure(caps, 0);
    gst_structure_get_int(structure, "width", &width);
    gst_structure_get_int(structure, "height", &height);
    gst_caps_unref(caps);

    // Update header information
    //       if (use_gst_timestamps_) {
    // #if (GST_VERSION_MAJOR == 1)
    //           cinfo->header.stamp =
    //           ros::Time(GST_TIME_AS_USECONDS(buf->pts+bt)/1e6+time_offset_);
    // #else
    //           cinfo->header.stamp =
    //           ros::Time(GST_TIME_AS_USECONDS(buf->timestamp+bt)/1e6+time_offset_);
    // #endif
    //       } else {
    //           cinfo->header.stamp = ros::Time::now();
    //       }

    // \todo   Should adapt to bit depth of pixformat;  just fixed for now
    const int bitDepth = 4;
    const unsigned int expected_frame_size = width * height * bitDepth;

    if (buf_size < expected_frame_size) {
      std::cerr << "GStreamer image buffer underflow: Expected frame to be "
                << expected_frame_size << " bytes but received just "
                << (buf_size)
                << " bytes. (make sure frames are correctly encoded)"
                << std::endl;
    }

    {
      // Wrap this in a buffer

      VPIImageData data;
      data.bufferType = VPI_IMAGE_BUFFER_HOST_PITCH_LINEAR;
      data.buffer.pitch.numPlanes = 1;
      data.buffer.pitch.planes[0].width = width;
      data.buffer.pitch.planes[0].height = height;
      data.buffer.pitch.planes[0].pitchBytes = width * bitDepth;
      data.buffer.pitch.planes[0].data = buf_data;

      if (pixformat() == V4L2_PIX_FMT_XBGR32)
        data.buffer.pitch.format = VPI_IMAGE_FORMAT_BGRA8;
      else {
        std::cerr << "Can't handle format " << pixformat() << std::endl;
        goto end;
      }

      {
        Camera::Buffer_t buffer;
        CHECK_STATUS(vpiImageCreateWrapper(&data, nullptr,
                                           VPI_BACKEND_CPU | VPI_BACKEND_CUDA,
                                           &(buffer.image)));

        buffer.driver_timestamp = frameTime;

        // Lazy timespec -> timeval conversion
        auto const ts = timespec_from_double(
            GST_TIME_AS_USECONDS(buf->pts + bt) / 1e6 + time_offset_);
        buffer.kernel_timestamp.tv_sec = ts.tv_sec;
        buffer.kernel_timestamp.tv_usec = ts.tv_nsec / 1000;

        if (_flash) std::cerr << _flash << std::flush;

        for (auto &callback : image_callbacks_) {
          callback(buffer);
        }

        ++_count;

        vpiImageDestroy(buffer.image);
      }
    }

  end:

    gst_memory_unmap(memory, &info);
    gst_memory_unref(memory);

    // Funny .. don't unref this?  I guess it's associated with sample
    // gst_buffer_unref(buf);
    gst_sample_unref(sample);
  }

  stopStreaming();
  if (err < 0) {
    std::cerr << "Error stopping capture: " << err << std::endl;
  }

  std::cerr << "Exiting threadLoop" << std::endl;
}

int GstCamera::startStreaming(void) {
  // Pre-roll camera if needed
  if (preroll_) {
    // The PAUSE, PLAY, PAUSE, PLAY cycle is to ensure proper pre-roll
    // I am told this is needed and am erring on the side of caution.
    gst_element_set_state(_pipeline, GST_STATE_PLAYING);
    if (gst_element_get_state(_pipeline, NULL, NULL, -1) ==
        GST_STATE_CHANGE_FAILURE) {
      std::cerr << "Failed to PLAY during preroll." << std::endl;
      return -1;
    }

    gst_element_set_state(_pipeline, GST_STATE_PAUSED);
    if (gst_element_get_state(_pipeline, NULL, NULL, -1) ==
        GST_STATE_CHANGE_FAILURE) {
      std::cerr << "Failed to PAUSE." << std::endl;
      return -1;
    }
  }

  // Calibration between wall clock and gst timestamps
  GstClock *clock = gst_system_clock_obtain();
  GstClockTime ct = gst_clock_get_time(clock);
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  gst_object_unref(clock);
  time_offset_ = timespec_to_double(now) - GST_TIME_AS_USECONDS(ct) / 1e6;

  if (gst_element_set_state(_pipeline, GST_STATE_PLAYING) ==
      GST_STATE_CHANGE_FAILURE) {
    std::cerr << "Could not start stream!" << std::endl;
    return -1;
  }

  return 0;
}

void GstCamera::stopStreaming(void) {
  std::cerr << "Stopping gstreamer pipeline..." << std::endl;

  // Code cribbed from OpenCV:
  //    modules/videoio/src/cap_gstreamer.cpp#L907
  if (!_pipeline || !GST_IS_ELEMENT(_pipeline)) {
    std::cerr << "GStreamer: pipeline have not been created" << std::endl;
    return;
  }

  if (gst_element_set_state(_pipeline, GST_STATE_NULL) ==
      GST_STATE_CHANGE_FAILURE) {
    std::cerr << "unable to stop pipeline" << std::endl;
    gst_object_unref(GST_OBJECT(_pipeline));
    _pipeline = NULL;
  }
}

}  // namespace libtrisect
