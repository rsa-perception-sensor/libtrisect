// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include <sys/prctl.h>

#include "libtrisect/utils/set_thread_name.h"

namespace libtrisect {

void setThreadName(const char* threadName) {
  prctl(PR_SET_NAME, threadName, 0, 0, 0);
}

// Alternate method using pthread API
//    auto handle = thread->native_handle();
//    pthread_setname_np(handle,threadName);

}  // namespace libtrisect
