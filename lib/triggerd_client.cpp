// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include "libtrisect/trigger/triggerd_client.h"

#include <clocale>
#include <iostream>

namespace libtrisect {

bool minimal_dispatch(uint32_t id, ipc::in_message& in_msg,
                      ipc::out_message& out_msg) {
  return false;
}

auto minimal_predicate = []() { return true; };

TriggerdClient::TriggerdClient(const std::string& path)
    : TriggerService(), _socket_path(path) {}

int32_t TriggerdClient::get_period_ns() {
  try {
    return ipc::service_invoker()
        .call_by_address<(uint32_t)triggerd_function_t::get_period_ns, int32_t>(
            std::make_tuple(_socket_path), minimal_dispatch, minimal_predicate);
  } catch (const std::exception& ex) {
    std::cout << "error >> " << ex.what() << std::endl;
    return -1;
  }
}

int32_t TriggerdClient::set_period_ns(int32_t ns) {
  try {
    return ipc::service_invoker()
        .call_by_address<(uint32_t)triggerd_function_t::set_period_ns, int32_t>(
            std::make_tuple(_socket_path), minimal_dispatch, minimal_predicate,
            ns);
  } catch (const std::exception& ex) {
    std::cout << "error >> " << ex.what() << std::endl;
    return -1;
  }
}

}  // namespace libtrisect
