// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include "libtrisect/v4l_camera.h"

#include <avt_csi.h>
#include <cuda_runtime.h>
#include <libcsi_ioctl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <iostream>

#include "libtrisect/trigger/trigger_action.h"
#include "libtrisect/utils/set_thread_name.h"
#include "libtrisect/vpi_utils.h"

namespace libtrisect {

int xioctl(int fd, unsigned int request) {
  int r;
  if (fd < 0) return -1;

  do {
    r = ioctl(fd, request);
  } while (-1 == r && EINTR == errno);

  return r;
}

int xioctl(int fd, unsigned int request, void *arg) {
  int r;
  if (fd < 0) return -1;

  do {
    r = ioctl(fd, request, arg);
  } while (-1 == r && EINTR == errno);

  return r;
}

// Template specializations
template <>
uint8_t V4LCamera::bytetohost(uint8_t in) {
  return in;
}

template <>
uint16_t V4LCamera::bytetohost(uint16_t in) {
  return be16toh(in);
}

template <>
uint32_t V4LCamera::bytetohost(uint32_t in) {
  return be32toh(in);
}

template <>
uint64_t V4LCamera::bytetohost(uint64_t in) {
  return be64toh(in);
}

//================

V4LCamera::V4LCamera(const std::string &device, int fd, unsigned int width,
                     unsigned int height, uint32_t pixformat,
                     GpioConfig trigger_gpio)
    : Camera(width, height, pixformat),
      _device(device),
      _fd(fd),
      _doAutoGain(false),
      _doAutoExposure(false),
      trigger_gpio_(trigger_gpio),
      _buffers() {}

V4LCamera::~V4LCamera() {
  setDone();
  join();

  clearBuffers();

  if (-1 == close(_fd))
    std::cerr << "Error closing fd " << _fd << " for " << _device << " ("
              << errno << "): " << strerror(errno)
              << std::endl;  // errno_exit("munmap");

  _fd = -1;
}

std::shared_ptr<TriggerAction> V4LCamera::configureTrigger(TriggerType t) {
  if (t == TriggerType::TRIGGERD) {
    this->enableHardwareTrigger();
    return nullptr;
  } else if (t == TriggerType::GPIO) {
    this->enableHardwareTrigger();
    return this->hardwareTriggerAction();
  } else if (t == TriggerType::SOFTWARE) {
    this->enableSoftwareTrigger();
    return this->softwareTriggerAction();
  } else if (t == TriggerType::DISABLED) {
    this->disableHardwareTrigger();
    return nullptr;
  }

  assert("Unable to handle trigger type");
  return nullptr;
}

void V4LCamera::clearBuffers() {
  for (auto &buffer : _buffers) {
    vpiImageDestroy(buffer.image);
    cudaFree(buffer.userptr);
  }
  _buffers.clear();
}

int V4LCamera::fdioctl(unsigned int request) { return xioctl(_fd, request); }

int V4LCamera::fdioctl(unsigned int request, void *arg) {
  return xioctl(_fd, request, arg);
}

// int V4LCamera::setUserExposureFunction(ExposureFunction expFunction) {
//   auto ret = setAutoExposure(false);
//   if (ret < 0) return ret;

//   _userExposureFunction = expFunction;
//   return 0;
// }

int V4LCamera::initMemoryMap(void) {
  // Query image size
  int buffer_size;

  {
    v4l2_format fmt;
    CLEAR(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (fdioctl(VIDIOC_G_FMT, &fmt) < 0) {
      std::cerr << "Error querying format" << std::endl;
      return -1;
    }

    /* From code sample: Buggy driver paranoia. */
    unsigned int min = fmt.fmt.pix.width * 2;
    if (fmt.fmt.pix.bytesperline < min) fmt.fmt.pix.bytesperline = min;
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if (fmt.fmt.pix.sizeimage < min) fmt.fmt.pix.sizeimage = min;

    std::cout << "sizeimage: " << fmt.fmt.pix.sizeimage << std::endl;

    buffer_size = fmt.fmt.pix.sizeimage;
  }

  unsigned int page_size;
  page_size = getpagesize();
  buffer_size = (buffer_size + page_size - 1) & ~(page_size - 1);

  struct v4l2_requestbuffers req;
  CLEAR(req);

  const size_t num_buf_requested = 8;

  req.count = num_buf_requested;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_USERPTR;

  if (fdioctl(VIDIOC_REQBUFS, &req) != 0) {
    std::cerr << _device
              << " does not support "
                 "user pointer i/o ("
              << errno << "): " << strerror(errno) << std::endl;
    return -1;
  }

  if (req.count < 2) {
    std::cerr << "Insufficient buffer memory on " << _device << std::endl;
    return -2;
  } else if (req.count != num_buf_requested) {
    std::cerr << "Requested " << num_buf_requested
              << " buffers, V4L only allocated " << req.count << std::endl;
  }

  std::cout << "Allocating " << req.count << " buffers" << std::endl;

  _buffers.resize(req.count);

  for (unsigned int i = 0; i < _buffers.size(); ++i) {
    // _buffers[i].length = buffer_size;

    uint8_t *ptr;
    cudaMallocManaged((void **)&ptr, buffer_size, cudaMemAttachGlobal);

    if (!ptr) {
      std::cerr << "Out of memory" << std::endl;
      return -1;
    }

    VPIImageData data = {};
    data.bufferType = VPI_IMAGE_BUFFER_CUDA_PITCH_LINEAR;
    data.buffer.pitch.numPlanes = 1;
    data.buffer.pitch.planes[0].width = width();
    data.buffer.pitch.planes[0].height = height();
    data.buffer.pitch.planes[0].pitchBytes = width();

    data.buffer.pitch.planes[0].data = ptr;
    data.buffer.pitch.format = VPI_IMAGE_FORMAT_U8;

    CHECK_STATUS(vpiImageCreateWrapper(&data, nullptr,
                                       VPI_BACKEND_CPU | VPI_BACKEND_CUDA,
                                       &(_buffers[i].image)));

    // Convenience copies ...
    _buffers[i].userptr = ptr;
    _buffers[i].length = buffer_size;
  }

  return 0;
}

int V4LCamera::readFrame(void) {
  timespec frameTime;
  clock_gettime(CLOCK_REALTIME, &frameTime);

  struct v4l2_buffer buf;
  CLEAR(buf);

  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_USERPTR;

  // Get buffer from driver
  if (-1 == fdioctl(VIDIOC_DQBUF, &buf)) {
    std::cerr << _device << ": Got error while dequeueing buffer " << errno
              << " " << strerror(errno) << std::endl;
    switch (errno) {
      case EAGAIN:
        return 0;

      case EIO:
        /* Could ignore EIO, see spec. */
        /* fall through */

      default:
          // This is an error condition, but we aren't handling it
          // properly (yet).  The original code was:
          //  errno_exit("VIDIOC_DQBUF");
          ;
    }
  }

  // Figure out which buffer was used
  size_t i;
  for (i = 0; i < _buffers.size(); ++i) {
    if ((uint8_t *)buf.m.userptr == _buffers[i].userptr) break;
  }

  if (i >= _buffers.size()) {
    std::cerr << "Buffer index " << i << " greater than length of _buffers "
              << _buffers.size() << std::endl;
    return -1;
  }
  assert(i < _buffers.size());

  if (_flash) std::cerr << _flash << std::flush;

  // \todo Is it necessary to validate image height, width
  // and format before passing to the user?

  _buffers[i].kernel_timestamp = buf.timestamp;
  _buffers[i].driver_timestamp = frameTime;

  cudaStreamAttachMemAsync(NULL, _buffers[i].userptr, 0, cudaMemAttachHost);

  for (auto &callback : image_callbacks_) {
    callback(_buffers[i]);
  }

  ++_count;

  // Release buffer back to driver
  if (-1 == fdioctl(VIDIOC_QBUF, &buf))
    ;

  return 1;
}

void V4LCamera::threadLoop(void) {
  setThreadName("V4LCamera::threadLoop");

  auto err = startCapturing();
  if (err < 0) {
    std::cerr << _device << "Error starting capture: " << err << std::endl;
    return;
  }

  while (!done()) {
    fd_set fds;
    struct timeval tv;
    int r;

    FD_ZERO(&fds);
    FD_SET(_fd, &fds);

    /* Timeout. */
    tv.tv_sec = 1;
    tv.tv_usec = 0;

    r = select(_fd + 1, &fds, NULL, NULL, &tv);

    if (-1 == r) {
      // if (EINTR == errno)
      continue;
    }

    if (0 == r) {
      std::cerr << _device << " : Camera timeout" << std::endl;
      continue;
    }

    readFrame();
  }

  std::cerr << _device << " : Shutting down threadLoop" << std::endl;

  err = stopCapturing();
  if (err < 0) {
    std::cerr << "Error stopping capture: " << err << std::endl;
  }

  std::cerr << _device << " : Exit threadLoop" << std::endl;
}

int V4LCamera::startCapturing(void) {
  unsigned int i;
  enum v4l2_buf_type type;

  for (i = 0; i < _buffers.size(); ++i) {
    struct v4l2_buffer buf;

    CLEAR(buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_USERPTR;
    buf.index = i;
    buf.m.userptr = (unsigned long)_buffers[i].userptr;
    buf.length = _buffers[i].length;

    if (-1 == fdioctl(VIDIOC_QBUF, &buf)) return errno;
  }

  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == fdioctl(VIDIOC_STREAMON, &type)) return errno;

  return 0;
}

int V4LCamera::stopCapturing(void) {
  enum v4l2_buf_type type;

  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == fdioctl(VIDIOC_STREAMOFF, &type)) return errno;

  return 0;
}

//===================================================================
//
// Configuration via direct V4L2 ioctl

// --- Functions to get/set framerate --

int V4LCamera::setFrameRate(int numerator, int denominator) {
  struct v4l2_streamparm parm;

  parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  parm.parm.capture.timeperframe.numerator = numerator;
  parm.parm.capture.timeperframe.denominator = denominator;

  if (-1 == fdioctl(VIDIOC_S_PARM, &parm)) {
    return -1;
  }

  return 0;
}

float V4LCamera::fps(void) {
  struct v4l2_streamparm parm;

  parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == fdioctl(VIDIOC_G_PARM, &parm)) {
    return -1;
  }

  return parm.parm.capture.timeperframe.denominator * 1.0 /
         parm.parm.capture.timeperframe.numerator;
}

int V4LCamera::disableHardwareTrigger() {
  int err = setV4lUserControl(V4L2_CID_TRIGGER_MODE, 0);

  if (err != 0) {
    std::cerr << "Error disabling trigger (" << err << "): " << strerror(err)
              << std::endl;
  }

  return err;

  // if (-1 == fdioctl(VIDIOC_TRIGGER_MODE_OFF)) {
  //   if (EINVAL == errno) {
  //     fprintf(stderr, "%s is not a V4L2 device\n", _device.c_str());
  //     return errno;
  //   } else {
  //     fprintf(stderr, "Unable to disable trigger\n");
  //     return errno;  // errno_exit("VIDIOC_S_TRIGGER_MODE_{ON|OFF}");
  //   }
  // }
  // return 0;
}

int V4LCamera::enableV4LTrigger(int trigger, int mode) {
  if (setV4lUserControl(V4L2_CID_TRIGGER_MODE, 1) != 0) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is not a V4L2 device\n", _device.c_str());
      return errno;
    } else {
      std::cerr << "Unable to enable trigger for " << _device << " (" << errno
                << "): " << strerror(errno) << std::endl;
      return errno;
    }
  }

  std::cerr << "Setting trigger source for " << _device << " to " << trigger
            << std::endl;
  if (setV4lUserControl(V4L2_CID_TRIGGER_SOURCE, trigger) != 0) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is no V4L2 device\n", _device.c_str());
      return errno;
    } else {
      std::cerr << "Unable to enable trigger type  " << trigger << " for "
                << _device << " (" << errno << "): " << strerror(errno)
                << std::endl;
      return errno;  // errno_exit("VIDIOC_QUERYCAP");
    }
  }

  if (mode >= 0) {
    if (setV4lUserControl(V4L2_CID_TRIGGER_ACTIVATION, mode) != 0) {
      if (EINVAL == errno) {
        fprintf(stderr, "%s is not a V4L2 device\n", _device.c_str());
        return errno;
      } else {
        std::cerr << "Unable to set trigger activation for " << _device << " ("
                  << errno << "): " << strerror(errno) << std::endl;
        return errno;  // errno_exit("VIDIOC_S_TRIGGER_ACTIVATION");
      }
    }
  }

  return 0;
}

int V4LCamera::getTriggerSource(void) {
  return getV4lUserControl(V4L2_CID_TRIGGER_SOURCE);
}

int V4LCamera::getTriggerActivation(void) {
  return getV4lUserControl(V4L2_CID_TRIGGER_ACTIVATION);
}

int V4LCamera::softwareTrigger() {
  return setV4lUserControl(V4L2_CID_TRIGGER_SOFTWARE, 0);
}

//===================================================================
//
// Configuration via V4L2 user control

v4l2_query_ext_ctrl V4LCamera::queryV4LUserControl(uint32_t param) {
  struct v4l2_query_ext_ctrl queryctrl;
  memset(&queryctrl, 0, sizeof(v4l2_query_ext_ctrl));
  queryctrl.id = param;

  if (-1 == fdioctl(VIDIOC_QUERY_EXT_CTRL, &queryctrl)) {
    std::cerr << "Querying param " << std::hex << param << std::dec
              << " not supported for " << _device << " (" << errno
              << "): " << strerror(errno) << std::endl;

    queryctrl.id = 0;
    return queryctrl;
  } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
    std::cerr << "Control flag " << queryctrl.name << " disabled " << _device
              << std::endl;
  }

  // std::cerr << "Param " << queryctrl.name << " minimum " << queryctrl.minimum
  // << " maximum " << queryctrl.maximum << std::endl; std::cerr << "Param " <<
  // queryctrl.name << "type " << std::hex << queryctrl.type << " flags " <<
  // queryctrl.flags << std::dec << std::endl;

  return queryctrl;
}

//* The Generic "get V4l control function"
int V4LCamera::getV4lUserControl(uint32_t param) {
  struct v4l2_query_ext_ctrl queryctrl = queryV4LUserControl(param);

  struct v4l2_control control;
  memset(&control, 0, sizeof(control));
  control.id = param;

  if (-1 == fdioctl(VIDIOC_G_CTRL, &control)) {
    std::cerr << "Error getting param " << queryctrl.name << "(0x" << std::hex
              << param << std::dec << ") for " << _device << " (" << errno
              << "): " << strerror(errno) << std::endl;
    return -1;
  }

  return control.value;
}

// \todo{amarburg} Reduce the DRY
int64_t V4LCamera::getV4lUserControl64(uint32_t param) {
  struct v4l2_query_ext_ctrl queryctrl = queryV4LUserControl(param);

  if (queryctrl.id != param) return -1;

  v4l2_ext_controls controls;
  v4l2_ext_control control;

  memset(&controls, 0, sizeof(v4l2_ext_controls));
  memset(&control, 0, sizeof(v4l2_ext_control));

  controls.count = 1;
  controls.which = V4L2_CTRL_WHICH_CUR_VAL;
  controls.ctrl_class = V4L2_CTRL_CLASS_USER;
  controls.controls = &control;

  control.id = param;

  if (-1 == fdioctl(VIDIOC_G_EXT_CTRLS, &controls)) {
    std::cerr << "Error getting param " << queryctrl.name << "(0x" << std::hex
              << param << std::dec << ") for " << _device << " (" << errno
              << "): " << strerror(errno) << std::endl;
    return -1;
  }

  return control.value64;
}

int V4LCamera::setV4lUserControl(uint32_t param, int value) {
  struct v4l2_query_ext_ctrl queryctrl = queryV4LUserControl(param);

  struct v4l2_control control;
  memset(&control, 0, sizeof(control));
  control.id = param;
  control.value = value;

  if (-1 == fdioctl(VIDIOC_S_CTRL, &control)) {
    std::cerr << "Error setting param " << queryctrl.name << " to " << value
              << " for " << _device << " (" << errno << "): " << strerror(errno)
              << std::endl;
    return -1;
  }

  return 0;
}

int V4LCamera::setV4lUserControl64(uint32_t param, int64_t value) {
  struct v4l2_query_ext_ctrl queryctrl = queryV4LUserControl(param);

  if (queryctrl.id != param) return -1;

  v4l2_ext_controls controls;
  v4l2_ext_control control;

  memset(&controls, 0, sizeof(v4l2_ext_controls));
  memset(&control, 0, sizeof(v4l2_ext_control));

  controls.count = 1;
  controls.which = V4L2_CTRL_WHICH_CUR_VAL;
  controls.ctrl_class = V4L2_CTRL_CLASS_USER;
  controls.controls = &control;

  control.id = param;
  control.value64 = value;

  if (fdioctl(VIDIOC_TRY_EXT_CTRLS, &controls) < 0) {
    std::cerr << "Error trying param " << queryctrl.name << "(0x" << std::hex
              << param << std::dec << ") for " << _device << " (" << errno
              << "): " << strerror(errno) << std::endl;
    return -1;
  }

  if (fdioctl(VIDIOC_S_EXT_CTRLS, &controls) < 0) {
    std::cerr << "Error setting param " << queryctrl.name << "(0x" << std::hex
              << param << std::dec << ") for " << _device << " (" << errno
              << "): " << strerror(errno) << std::endl;
    return -1;
  }

  return 0;
}

int V4LCamera::setFlip(bool horizontal, bool vertical) {
  auto reth = setV4lUserControl(V4L2_CID_HFLIP, horizontal ? 1 : 0);
  auto retv = setV4lUserControl(V4L2_CID_VFLIP, vertical ? 1 : 0);

  return std::min(retv, reth);
}

int V4LCamera::setAutoExposure(bool enable) {
  _doAutoExposure = enable;
  // n.b. autoexposure is 0 == auto, 1 == manual
  return setV4lUserControl(V4L2_CID_EXPOSURE_AUTO, enable ? 0 : 1);
}

Camera::ExposureType V4LCamera::exposure(void) {
  return getV4lUserControl64(V4L2_CID_EXPOSURE);
}

int V4LCamera::setExposure(Camera::ExposureType exp) {
  if (exp < 0) return -1;
  return setV4lUserControl64(V4L2_CID_EXPOSURE, exp);
}

V4LCamera::ExposureMinMax V4LCamera::exposureBounds(void) {
  return getV4LBounds<Camera::ExposureType>(V4L2_CID_EXPOSURE);
}

int V4LCamera::setAutoGain(bool enable) {
  _doAutoGain = enable;
  return setV4lUserControl(V4L2_CID_AUTOGAIN, enable ? 1 : 0);
}

Camera::GainType V4LCamera::gain(void) {
  auto g = getV4lUserControl64(V4L2_CID_GAIN);

  return g;
  // if ((g != _lastGain) && (_lastGain > 0))
  //   std::cerr << "Gain from kernel not equal to last gain set " << g
  //             << " != " << _lastGain << std::endl;
  // return _lastGain;
}

int V4LCamera::setGain(Camera::GainType i) {
  //  _lastGain = i;
  return setV4lUserControl64(V4L2_CID_GAIN, i);
}

V4LCamera::GainMinMax V4LCamera::gainBounds(void) {
  return getV4LBounds<Camera::GainType>(V4L2_CID_GAIN);
}

//=== Direct access to Alvium registers ============================
//=== (via the VIDEOC_R_I2C and VIDEOC_W_I2C ioctls)

#define ALVIUM_REG_SIZE 2

// Aboslute addresses to I2C registers on Alvium camera
#define ALVIUM_V4L2_REGISTER_ADDRESS 0x0014
#define ALVIUM_DEVICE_GUID 0x0018
#define ALVIUM_MANUFACTURER_NAME 0x0058
#define ALVIUM_MODEL_NAME 0x0098
#define ALVIUM_FAMILY_NAME 0x00D8
#define ALVIUM_DEVICE_VERSION 0x0118
#define ALVIUM_MANUFACTURER_INFO 0x0158
#define ALVIUM_SERIAL_NUMBER 0x0198

// Register in the V4L register map which is at an offset
// given in register ALVIUM_V4L2_REGISTER_ADDRESS
//
#define ALVIUM_V4L_REGISTER_MAP_VERSION 0x0000

#define ALVIUM_V4L_DEVICE_FIRMWARE 0x0010
#define ALVIUM_V4L_CSI2_LANE_COUNT 0x0044

#define ALVIUM_V4L_WIDTH 0x0100
#define ALVIUM_V4L_HEIGHT 0x0110

#define ALVIUM_V4L_MIPI_DATA_FORMAT 0x0140

#define ALVIUM_DATA_FORMAT_YUV422 0x1E
#define ALVIUM_DATA_FORMAT_RGB8 0x24
#define ALVIUM_DATA_FORMAT_RAW8 0x2A
#define ALVIUM_DATA_FORMAT_RAW10 0x2B
#define ALVIUM_DATA_FORMAT_RAW12 0x2C

#define ALVIUM_V4L_AVAILABLE_MIPI_DATA_FORMATS 0x0148

#define ALVIUM_V4L_EXPOSURE_TIME 0x0180
#define ALVIUM_V4L_EXPOSURE_TIME_MIN 0x0188
#define ALVIUM_V4L_EXPOSURE_TIME_MAX 0x0190

#define ALVIUM_V4L_EXPOSURE_AUTO_MIN 0x0330
#define ALVIUM_V4L_EXPOSURE_AUTO_MAX 0x0338

#define ALVIUM_V4L_GAIN 0x01C8
#define ALVIUM_V4L_GAIN_MIN 0x01D0
#define ALVIUM_V4L_GAIN_MAX 0x01D8

#define ALVIUM_V4L_TEMPERATURE 0x0310

int V4LCamera::getI2CRegister(uint32_t reg, void *buffer, size_t bytes) {
  //     struct v4l2_i2c
  // {
  //     __u32       register_address;       // Register
  //     __u32       timeout;                // Timeout value
  //     const char* ptr_buffer;             // I/O buffer
  //     __u32       register_size;          // Register size
  //     __u32       num_bytes;              // Bytes to read
  // };

  //    std::cerr << "Reading register " << std::hex << reg << std::endl;

  struct v4l2_i2c msg;
  msg.register_address = reg;
  msg.ptr_buffer = reinterpret_cast<const char *>(buffer);
  msg.register_size = ALVIUM_REG_SIZE;  // Fixed for Alvium cameras
  msg.num_bytes = bytes;

  if (-1 == fdioctl(VIDIOC_R_I2C, &msg)) {
    if (errno != EINVAL) {
      // perror("VIDIOC_QUERYCTRL");
      // exit(EXIT_FAILURE);
      return -1;
    } else {
      // printf("V4L2_CID_BRIGHTNESS is not supportedn");
      return -1;
    }
  }

  return 0;
}

int V4LCamera::setI2CRegister(uint32_t reg, void *buffer, size_t bytes) {
  struct v4l2_i2c msg;
  msg.register_address = reg;
  msg.ptr_buffer = reinterpret_cast<const char *>(buffer);
  msg.register_size = ALVIUM_REG_SIZE;  // Fixed for Alvium cameras
  msg.num_bytes = bytes;

  if (-1 == fdioctl(VIDIOC_W_I2C, &msg)) {
    if (errno != EINVAL) {
      // perror("VIDIOC_QUERYCTRL");
      // exit(EXIT_FAILURE);
      return -1;
    } else {
      // printf("V4L2_CID_BRIGHTNESS is not supportedn");
      return -1;
    }
  }

  return 0;
}

std::string V4LCamera::getI2CString(uint32_t reg) {
  char buffer[64];

  if (getI2CRegister(reg, buffer, 64)) {
    return "";
  }

  return std::string(buffer);
}

uint16_t V4LCamera::getV4L2RegisterOffset() {
  return getI2CRegister<uint16_t>(ALVIUM_V4L2_REGISTER_ADDRESS);
}

std::string V4LCamera::getDeviceGuid() {
  return getI2CString(ALVIUM_DEVICE_GUID);
}

std::string V4LCamera::getManufacturerName() {
  return getI2CString(ALVIUM_MANUFACTURER_NAME);
}

std::string V4LCamera::getModelName() {
  return getI2CString(ALVIUM_MODEL_NAME);
}

std::string V4LCamera::getFamilyName() {
  return getI2CString(ALVIUM_FAMILY_NAME);
}

std::string V4LCamera::getDeviceVersion() {
  return getI2CString(ALVIUM_DEVICE_VERSION);
}

std::string V4LCamera::getManufacturerInfo() {
  return getI2CString(ALVIUM_MANUFACTURER_INFO);
}

std::string V4LCamera::getSerialNumber() {
  return getI2CString(ALVIUM_SERIAL_NUMBER);
}

uint32_t V4LCamera::getV4LRegisterMapVersion() {
  return getV4LRegister<uint32_t>(ALVIUM_V4L_REGISTER_MAP_VERSION);
}

uint64_t V4LCamera::getFirmwareVersion() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_DEVICE_FIRMWARE);
}

uint8_t V4LCamera::getCSI2LaneCount() {
  return getV4LRegister<uint8_t>(ALVIUM_V4L_CSI2_LANE_COUNT);
}

uint32_t V4LCamera::getMIPIDataFormat() {
  return getV4LRegister<uint32_t>(ALVIUM_V4L_MIPI_DATA_FORMAT);
}

uint64_t V4LCamera::getAvailableMIPIDataFormats() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_AVAILABLE_MIPI_DATA_FORMATS);
}

uint64_t V4LCamera::getExposure() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_EXPOSURE_TIME);
}

uint64_t V4LCamera::getExposureMin() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_EXPOSURE_TIME_MIN);
}

uint64_t V4LCamera::getExposureMax() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_EXPOSURE_TIME_MAX);
}

uint64_t V4LCamera::getExposureAutoMin() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_EXPOSURE_AUTO_MIN);
}

uint64_t V4LCamera::getExposureAutoMax() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_EXPOSURE_AUTO_MAX);
}

uint32_t V4LCamera::getV4LWidth() {
  return getV4LRegister<uint32_t>(ALVIUM_V4L_WIDTH);
}

uint32_t V4LCamera::getV4LHeight() {
  return getV4LRegister<uint32_t>(ALVIUM_V4L_HEIGHT);
}

uint64_t V4LCamera::getGain() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_GAIN);
}

uint64_t V4LCamera::getGainMin() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_GAIN_MIN);
}

uint64_t V4LCamera::getGainMax() {
  return getV4LRegister<uint64_t>(ALVIUM_V4L_GAIN_MAX);
}

uint64_t V4LCamera::setGainRegister(uint64_t new_val) {
  return setV4LRegister<uint64_t>(ALVIUM_V4L_GAIN, new_val);
}

uint32_t V4LCamera::getTemperature() {
  return getV4LRegister<uint32_t>(ALVIUM_V4L_TEMPERATURE) / 10;
}

}  // namespace libtrisect
