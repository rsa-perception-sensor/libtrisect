// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include "libtrisect/camera.h"

#include <string.h>
#include <sys/mman.h>

#include <iostream>

namespace libtrisect {

//================

Camera::Camera(unsigned int width, unsigned int height, uint32_t pixformat)
    : _height(height),
      _width(width),
      _v4l2format(pixformat),
      _flash(0),
      _count(0),
      _done(false),
      _thread() {}

Camera::~Camera() {}

void Camera::addImageCallback(NewImageCallback callback) {
  image_callbacks_.push_back(callback);
}

void Camera::start(void) {
  _done = false;
  _thread = std::thread(&Camera::threadLoop, this);
}

}  // namespace libtrisect
