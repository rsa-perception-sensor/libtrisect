// Copyright (c) 2021, University of Washington
//
// Released under BSD 3-Clause license.  See LICENSE.

#include "libtrisect/trigger/trigger_service.h"

#include "libtrisect/trigger/trigger_loop.h"
#include "libtrisect/trigger/triggerd_client.h"

namespace libtrisect {

std::shared_ptr<TriggerService> MakeTriggerService(TriggerType type) {
  if (type == TriggerType::GPIO) {
    return std::make_shared<TriggerLoop>();
  } else if (type == TriggerType::TRIGGERD) {
    return std::make_shared<TriggerdClient>();
  } else if (type == TriggerType::SOFTWARE) {
    return std::make_shared<TriggerLoop>();
  }

  return std::make_shared<TriggerService>();
}

}  // namespace libtrisect
