// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#include <boost/program_options.hpp>
#include <chrono>
#include <iomanip>
using namespace std::chrono;

#include "libtrisect/trisect_cameras.h"

namespace po = boost::program_options;

using namespace libtrisect;

int colorCount = 0;
bool g_stop = false;

static void clear_signal_handlers() noexcept {
  signal(SIGINT, SIG_IGN);
  signal(SIGTERM, SIG_IGN);
}

static void install_signal_handlers(void (*handler)(int)) noexcept {
  signal(SIGINT, handler);
  signal(SIGTERM, handler);
}

static void signal_handler(int /*signum*/) noexcept {
  clear_signal_handlers();

  std::cerr << "Stopping cameras" << std::endl;
  g_stop = true;
}

void colorCallback(const libtrisect::Camera::Buffer_t &buffer) {
  int32_t width, height;
  vpiImageGetSize(buffer.image, &width, &height);

  std::cerr << std::setw(4) << colorCount << " : Got " << width << " x "
            << height << " image" << std::endl;
  ++colorCount;
}

int main(int argc, char **argv) {
  install_signal_handlers(signal_handler);

  bool isDrysect = false;

  try {
    // Parse command line options
    po::options_description options("Options");
    // clang-format off
    options.add_options()
        ("drysect", po::bool_switch(&isDrysect), "Use Drysect configuration")
        ("help", "produce help message");
    // clang-format on

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      return 1;
    }
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
  }

  const int hz = 10;
  //  auto color = ColorCamera::CreateCamera(ColorCamera::RES_4032_3040, hz);

  GstCamera::SharedPtr _colorCam;

  std::shared_ptr<CamFactory> cam_factory;
  if (isDrysect) {
    cam_factory = std::make_shared<DrysectCameras>();
  } else {
    cam_factory = std::make_shared<TrisectCameras>();
  }

  auto camera =
      cam_factory->ColorCamera(ColorCamera::Resolution::RES_3840_2160, hz);
  if (_colorCam) {
    _colorCam = camera.value();
    std::cerr << "Initialized color camera..." << std::endl;
  } else {
    std::cerr << "Failed to create color camera (" << camera.error().err
              << "): " << camera.error().msg() << std::endl;
    return -1;
  }

  assert(_colorCam);

  _colorCam->addImageCallback(colorCallback);

  auto start = high_resolution_clock::now();

  _colorCam->start();

  while (!g_stop) {
    sleep(5);
  }

  auto stop = high_resolution_clock::now();

  _colorCam->setDone();
  _colorCam->join();

  auto duration = duration_cast<milliseconds>(stop - start);

  std::cout << "Color camera captured " << _colorCam->count() << " frames"
            << " in " << duration.count() / 1000.0 << " seconds"
            << " at " << (_colorCam->count() * 1000.0 / duration.count())
            << " fps" << std::endl;

  return 0;
}
