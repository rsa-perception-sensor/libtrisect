// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#include <boost/program_options.hpp>
#include <chrono>
#include <iomanip>
using namespace std::chrono;

#include "libtrisect/trisect_cameras.h"

namespace po = boost::program_options;
using namespace libtrisect;

void queryCamera(const std::string &name,
                 const std::shared_ptr<libtrisect::V4LCamera> &cam) {
  if (!cam) return;

  std::string getManufacturerInfo();
  std::string getSerialNumber();

  std::cerr << "               " << name << " GUID: " << cam->getDeviceGuid()
            << std::endl;
  std::cerr << "       " << name
            << " manufacturer: " << cam->getManufacturerName() << std::endl;
  std::cerr << "         " << name << " model name: " << cam->getModelName()
            << std::endl;
  std::cerr << "        " << name << " family name: " << cam->getFamilyName()
            << std::endl;
  std::cerr << "     " << name << " device version: " << cam->getDeviceVersion()
            << std::endl;
  std::cerr << "         " << name
            << " Manuf info: " << cam->getManufacturerInfo() << std::endl;
  std::cerr << "      " << name << " Serial Number: " << cam->getSerialNumber()
            << std::endl;

  std::cerr << "" << name << " V4L register offset: 0x" << std::hex
            << std::setw(8) << std::setfill('0')
            << static_cast<int>(cam->getV4L2RegisterOffset()) << std::dec
            << std::endl;

  uint32_t rm = cam->getV4LRegisterMapVersion();
  std::cerr << "" << name << " V4L Reg map version: " << std::hex << rm
            << std::dec << std::endl;

  uint64_t fw = cam->getFirmwareVersion();
  std::cerr << "   " << name << " Firmware Version: " << std::hex
            << static_cast<int>(fw & 0x00000000000000FF) << ":"
            << static_cast<int>((fw & 0x000000000000FF00) >> 8) << ":"
            << static_cast<int>((fw & 0x00000000FFFF0000) >> 16) << ":"
            << std::setw(8) << std::setfill('0')
            << static_cast<int>((fw & 0xFFFFFFFF00000000) >> 32) << std::dec
            << std::endl;

  std::cerr << " " << name << " CSI-2 lanes in use: "
            << static_cast<int>(cam->getCSI2LaneCount()) << std::endl;

  std::cerr << " " << name << " Available MIPI formats: " << std::hex
            << static_cast<int>(cam->getAvailableMIPIDataFormats())
            << std::endl;

  std::cerr << "" << name << " Current MIPI format: " << std::hex
            << static_cast<int>(cam->getMIPIDataFormat()) << std::endl;

  std::cerr << std::dec;

  std::cerr << "       " << name << " Current size: " << cam->getV4LWidth()
            << " x " << cam->getV4LHeight() << std::endl;

  std::cerr << "    " << name << " Exposure bounds: " << cam->getExposureMin()
            << " - " << cam->getExposureMax() << std::endl;
  std::cerr << "" << name
            << " Autoexposure bounds: " << cam->getExposureAutoMin() << " - "
            << cam->getExposureAutoMax() << std::endl;

  std::cerr << "   " << name
            << " Current exposure (via register): " << cam->getExposure()
            << std::endl;
  std::cerr << "   " << name
            << " Current exposure (via V4L): " << cam->exposure() << std::endl;

  std::cerr << "         " << name << " Gain bounds: " << cam->getGainMin()
            << " - " << cam->getGainMax() << std::endl;
  std::cerr << "        " << name
            << " Current gain (via register): " << cam->getGain() << std::endl;
  std::cerr << "        " << name << " Current gain (via V4L): " << cam->gain()
            << std::endl;

  const uint32_t new_gain = 600;
  std::cerr << "     !!! " << name << " Set gain to: " << new_gain
            << " --> return value " << cam->setGain(new_gain) << std::endl;

  std::cerr << "        " << name
            << " gain is now (via register): " << cam->getGain() << std::endl;
  std::cerr << "        " << name << " gain is now (via V4L): " << cam->gain()
            << std::endl;

  std::cerr << "" << name << " Current temperature: " << cam->getTemperature()
            << std::endl;
}

int main(int argc, char **argv) {
  bool isDrysect = false;

  try {
    // Parse command line options
    po::options_description options("Options");
    options.add_options()("drysect", po::bool_switch(&isDrysect),
                          "Use Drysect configuration")("help",
                                                       "produce help message");

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      return 1;
    }

  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
  }

  std::shared_ptr<libtrisect::V4LCamera> _leftCam, _rightCam;  //, _colorCam;

  std::shared_ptr<CamFactory> cam_factory;

  if (isDrysect) {
    cam_factory = std::make_shared<DrysectCameras>();
  } else {
    cam_factory = std::make_shared<TrisectCameras>();
  }

  {
    auto left = cam_factory->LeftCamera(MonoCamera::FORMAT_GREY8);
    if (left) {
      _leftCam = left.value();

      queryCamera("Left", _leftCam);

      std::cerr << "Initialized left camera..." << std::endl;

    } else {
      std::cerr << "Failed to create left camera (" << left.error().err
                << "): " << left.error().msg() << std::endl;
      return -1;
    }
  }

  {
    auto right = cam_factory->RightCamera(MonoCamera::FORMAT_GREY8);
    if (right) {
      _rightCam = right.value();

      queryCamera("Right", _rightCam);

      std::cerr << "Initialized right camera..." << std::endl;

    } else {
      std::cerr << "Failed to create right camera (" << right.error().err
                << "): " << right.error().msg() << std::endl;
      return -1;
    }
  }

  return 0;
}
