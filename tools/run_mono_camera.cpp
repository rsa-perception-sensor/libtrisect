// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#include <boost/program_options.hpp>
#include <chrono>
using namespace std::chrono;

#include "libtrisect/trigger/trigger_service.h"
#include "libtrisect/trisect_cameras.h"

namespace po = boost::program_options;
using namespace libtrisect;

bool g_stop = false;
int img_count = 0, rightCount = 0;

static void clear_signal_handlers() noexcept {
  signal(SIGINT, SIG_IGN);
  signal(SIGTERM, SIG_IGN);
}

static void install_signal_handlers(void (*handler)(int)) noexcept {
  signal(SIGINT, handler);
  signal(SIGTERM, handler);
}

static void signal_handler(int /*signum*/) noexcept {
  clear_signal_handlers();

  std::cerr << "Stopping cameras" << std::endl;
  g_stop = true;
}

void imageCallback(const libtrisect::Camera::Buffer_t &buffer) {
  ++img_count;

  int32_t width, height;
  vpiImageGetSize(buffer.image, &width, &height);

  std::cerr << "Image " << height << " x " << width << " size " << std::endl;
}

int main(int argc, char **argv) {
  install_signal_handlers(signal_handler);

  TriggerType trigger_type_ = TriggerType::DISABLED;

  MonoCamera::MonoDataFormat_t monoFormat = MonoCamera::FORMAT_GREY8;

  bool isDrysect = false;
  bool rightCamera = false;

  try {
    bool useTriggerd = false;
    bool useGpioTrigger = false;
    bool useSoftwareTrigger = false;

    int bitDepth = 8;

    // Parse command line options
    po::options_description options("Options");

    // clang-format off
    options.add_options()
        ("help", "produce help message")
        ("drysect", po::bool_switch(&isDrysect), "Use Drysect configuration")
        ("right", po::bool_switch(&rightCamera), "Create right camera")
        ("gpio-trigger", po::bool_switch(&useGpioTrigger), "Use GPIO hardware trigger")
        ("triggerd", po::bool_switch(&useTriggerd), "Use triggerd to trigger cameras")
        ("software-trigger", po::bool_switch(&useSoftwareTrigger), "Use software trigger  for cameras")
        ("bit-depth", po::value<int>(&bitDepth),
            "Set desired bit depth (8,10,12)");
    // clang-format on

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      return 1;
    }

    if (bitDepth == 8) {
      monoFormat = MonoCamera::FORMAT_GREY8;
    } else if (bitDepth == 10) {
      monoFormat = MonoCamera::FORMAT_GREY10;
    } else if (bitDepth == 12) {
      monoFormat = MonoCamera::FORMAT_GREY12;
    } else {
      std::cerr << "Unknown bit depth: " << bitDepth << std::endl;
      return -1;
    }

    if (useTriggerd) {
      trigger_type_ = TriggerType::TRIGGERD;
    } else if (useGpioTrigger) {
      trigger_type_ = TriggerType::GPIO;
    } else if (useSoftwareTrigger) {
      trigger_type_ = TriggerType::SOFTWARE;
    }

  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
  }

  std::shared_ptr<libtrisect::V4LCamera> camera_;

  const int gain = 1200;

  std::shared_ptr<CamFactory> cam_factory;

  if (isDrysect) {
    cam_factory = std::make_shared<DrysectCameras>();
  } else {
    cam_factory = std::make_shared<TrisectCameras>();
  }

  if (rightCamera) {
    auto cam = cam_factory->RightCamera(monoFormat);
    if (cam) {
      camera_ = cam.value();
    } else {
      std::cerr << "Failed to create right camera (" << cam.error().err
                << "): " << cam.error().msg() << std::endl;
      return -1;
    }
  } else {
    auto cam = cam_factory->LeftCamera(monoFormat);
    if (cam) {
      camera_ = cam.value();
    } else {
      std::cerr << "Failed to create left camera (" << cam.error().err
                << "): " << cam.error().msg() << std::endl;
      return -1;
    }
  }

  camera_->addImageCallback(imageCallback);
  camera_->setFlash('*');

  std::cerr << "Disabling auto gain: "
            << (camera_->setAutoGain(false) ? "error" : "ok") << std::endl;

  auto currentGain = camera_->gain();
  if (currentGain >= 0) {
    std::cerr << "Current gain is " << currentGain << std::endl;
  } else {
    std::cerr << " !! Error querying gain" << std::endl;
  }

  std::cerr << "Set gain to " << gain << ", result: " << camera_->setGain(gain)
            << std::endl;
  std::cerr << "Disabling auto exposure: " << camera_->setAutoExposure(false)
            << std::endl;

  std::cerr << "Initialized camera..." << std::endl;

  if (trigger_type_ == TriggerType::DISABLED) {
    std::cerr << "Trigger disabled, cameras will be free-running" << std::endl;
  }

  std::shared_ptr<TriggerService> trigger_service =
      libtrisect::MakeTriggerService(trigger_type_);
  trigger_service->addTrigger(camera_->configureTrigger(trigger_type_));
  trigger_service->setFps(10);

  auto start = high_resolution_clock::now();

  camera_->start();

  trigger_service->start();

  while (!g_stop) {
    sleep(5);

    auto duration =
        duration_cast<milliseconds>(high_resolution_clock::now() - start);
    if (camera_) {
      std::cout << "Left camera captured " << camera_->count() << " frames; "
                << (camera_->count() * 1000.0 / duration.count()) << " fps"
                << std::endl;
    }
  }

  if (camera_) {
    camera_->setDone();
    std::cerr << "Waiting for left camera to stop" << std::endl;
    camera_->join();
  }

  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<milliseconds>(stop - start);

  std::cout << "Captured images for " << duration.count() / 1000.0 << " sec"
            << std::endl;

  if (camera_) {
    std::cout << std::endl
              << "Left camera captured " << camera_->count() << " frames; "
              << (camera_->count() * 1000.0 / duration.count()) << " fps"
              << std::endl;
  }

  trigger_service->stop();
  std::cout << "Trigger fired " << trigger_service->count() << " times"
            << std::endl;

  return 0;
}
