// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#include "libtrisect/trigger/triggerd_client.h"

#include <boost/program_options.hpp>
#include <clocale>
#include <iostream>

namespace po = boost::program_options;

using namespace libtrisect;

int main(int argc, char **argv) {
  std::setlocale(LC_ALL, "");

  // Subcommand example taken from
  //  https://stackoverflow.com/questions/15541498/how-to-implement-subcommands-using-boost-program-options
  //
  po::options_description global("Allowed options");
  global.add_options()("help", "produce help message")(
      "command", po::value<std::string>()->required(), "set, get, help")(
      "subargs", po::value<std::vector<std::string>>(),
      "Arguments for command");

  po::positional_options_description pos;
  pos.add("command", 1).add("subargs", -1);

  po::variables_map vm;

  try {
    po::parsed_options parsed = po::command_line_parser(argc, argv)
                                    .options(global)
                                    .positional(pos)
                                    .allow_unregistered()
                                    .run();

    po::store(parsed, vm);

    if (vm.count("help") > 0) {
      std::cout << global << std::endl;
    } else if (vm.count("command") > 0) {
      TriggerdClient client;

      std::string cmd = vm["command"].as<std::string>();
      if (cmd == "set") {
        // Code snippet for sub-command options:
        po::options_description set_options("set options");
        set_options.add_options()("secs", po::value<float>()->required(),
                                  "Period in seconds");

        po::positional_options_description set_pos;
        set_pos.add("secs", 1);

        // Collect all the unrecognized options from the first pass. This will
        // include the (positional) command name, so we need to erase that.
        std::vector<std::string> opts =
            po::collect_unrecognized(parsed.options, po::include_positional);
        opts.erase(opts.begin());

        // Parse again...
        po::store(po::command_line_parser(opts)
                      .options(set_options)
                      .positional(set_pos)
                      .run(),
                  vm);

        float period = vm["secs"].as<float>();
        int32_t ns =
            std::min(static_cast<double>(INT_MAX), floor(period * 1e9));

        std::cerr << "Setting period to " << period << " seconds; " << ns
                  << " nanoseconds" << std::endl;

        auto result = client.set_period_ns(ns);

        std::cerr << "Received period " << result << " nanoseconds"
                  << std::endl;
      } else if (cmd == "get") {
        auto result = client.get_period_ns();
        std::cerr << "Received period " << result << " nanoseconds"
                  << std::endl;
      } else if (cmd == "stop") {
        std::cout << global << std::endl;
      } else {
        std::cout << "Unknown option \"" << cmd << "\"" << std::endl;
        return -1;
      }
    }

  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}
