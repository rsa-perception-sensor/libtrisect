// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#include <boost/program_options.hpp>
#include <chrono>
#include <mutex>
using namespace std::chrono;

#include "libtrisect/trigger/trigger_service.h"
#include "libtrisect/trisect_cameras.h"

namespace po = boost::program_options;
using namespace libtrisect;

bool g_stop = false;
int leftCount = 0, rightCount = 0;

static void clear_signal_handlers() noexcept {
  signal(SIGINT, SIG_IGN);
  signal(SIGTERM, SIG_IGN);
}

static void install_signal_handlers(void (*handler)(int)) noexcept {
  signal(SIGINT, handler);
  signal(SIGTERM, handler);
}

static void signal_handler(int /*signum*/) noexcept {
  clear_signal_handlers();

  std::cerr << "Stopping cameras" << std::endl;
  g_stop = true;
}

std::mutex callback_mutex_;

void leftCallback(const libtrisect::Camera::Buffer_t &buffer) {
  std::lock_guard<std::mutex> lock_guard(callback_mutex_);
  ++leftCount;
  double sec = buffer.time.tv_sec + (buffer.time.tv_nsec / 1e9);

  int32_t width, height;
  vpiImageGetSize(buffer.image, &width, &height);

  std::cerr << std::fixed << "(" << sec << ")  Left image " << height << " x "
            << width << " size " << std::endl;
}

void rightCallback(const libtrisect::Camera::Buffer_t &buffer) {
  std::lock_guard<std::mutex> lock_guard(callback_mutex_);
  ++rightCount;
  double sec = buffer.time.tv_sec + (buffer.time.tv_nsec / 1e9);

  int32_t width, height;
  vpiImageGetSize(buffer.image, &width, &height);

  std::cerr << std::fixed << "(" << sec << ") Right image " << height << " x "
            << width << " size " << std::endl;
}

int main(int argc, char **argv) {
  install_signal_handlers(signal_handler);

  MonoCamera::MonoDataFormat_t monoFormat = MonoCamera::FORMAT_GREY8;

  libtrisect::TriggerType trigger_type_ = TriggerType::DISABLED;

  bool isDrysect = false;

  try {
    bool useTriggerd = false;
    bool useGpioTrigger = false;
    bool useSoftwareTrigger = false;
    bool useNoTrigger = false;

    int bitDepth = 8;

    // Parse command line options
    po::options_description options("Options");
    // clang-format off
    options.add_options()
      ("help", "produce help message")
        ("drysect", po::bool_switch(&isDrysect), "Use Drysect configuration")
        ("no-trigger", po::bool_switch(&useNoTrigger), "No trigger (free-running)")
        ("gpio-trigger", po::bool_switch(&useGpioTrigger), "Use GPIO hardware trigger")
        ("triggerd", po::bool_switch(&useTriggerd), "Use triggerd to trigger cameras")
        ("software-trigger", po::bool_switch(&useSoftwareTrigger), "Use software trigger  for cameras")
      ("bit-depth", po::value<int>(&bitDepth),"Set desired bit depth (8,10,12)");
    // clang-format on

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      return 1;
    }

    if (bitDepth == 8) {
      monoFormat = MonoCamera::FORMAT_GREY8;
    } else if (bitDepth == 10) {
      monoFormat = MonoCamera::FORMAT_GREY10;
    } else if (bitDepth == 12) {
      monoFormat = MonoCamera::FORMAT_GREY12;
    } else {
      std::cerr << "Unknown bit depth: " << bitDepth << std::endl;
      return -1;
    }

    if (useTriggerd) {
      trigger_type_ = TriggerType::TRIGGERD;
    } else if (useGpioTrigger) {
      trigger_type_ = TriggerType::GPIO;
    } else if (useSoftwareTrigger) {
      trigger_type_ = TriggerType::SOFTWARE;
    } else {
      std::cerr << "No trigger specified" << std::endl;
      trigger_type_ = TriggerType::DISABLED;
    }

  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
  }

  std::shared_ptr<TriggerService> trigger_service;
  std::shared_ptr<libtrisect::V4LCamera> _leftCam, _rightCam;  //, _colorCam;

  const int gain = 2400;

  std::shared_ptr<CamFactory> cam_factory;
  if (isDrysect) {
    cam_factory = std::make_shared<DrysectCameras>();
  } else {
    cam_factory = std::make_shared<TrisectCameras>();
  }

  auto left = cam_factory->LeftCamera(monoFormat);
  if (left) {
    _leftCam = left.value();

    _leftCam->addImageCallback(leftCallback);

    std::cerr << "Disabling auto gain: "
              << (_leftCam->setAutoGain(false) ? "error" : "ok") << std::endl;
    std::cerr << "Set gain to " << gain
              << ", result: " << _leftCam->setGain(gain) << std::endl;
    std::cerr << "Disabling auto exposure: " << _leftCam->setAutoExposure(false)
              << std::endl;

    std::cerr << "Initialized left camera..." << std::endl;

  } else {
    std::cerr << "Failed to create left camera (" << left.error().err
              << "): " << left.error().msg() << std::endl;
    return -1;
  }

  auto right = cam_factory->RightCamera(monoFormat);
  if (right) {
    _rightCam = right.value();

    _rightCam->addImageCallback(rightCallback);

    std::cerr << "Disabling auto gain: "
              << (_rightCam->setAutoGain(false) ? "error" : "ok") << std::endl;
    std::cerr << "Set gain to " << gain
              << ", result: " << _rightCam->setGain(gain) << std::endl;

    // _rightCam->setUserExposureFunction(
    //     [&_leftCam](const Camera::Buffer_t &image) -> int {
    //       return _leftCam->exposure();
    //     });

    std::cerr << "Initialized right camera..." << std::endl;

  } else {
    std::cerr << "Failed to create right camera (" << right.error().err
              << "): " << right.error().msg() << std::endl;
    return -1;
  }

  if (trigger_type_ == TriggerType::DISABLED) {
    std::cerr << "Trigger disabled, cameras will be free-running" << std::endl;
  }

  trigger_service = libtrisect::MakeTriggerService(trigger_type_);

  if (_leftCam)
    trigger_service->addTrigger(_leftCam->configureTrigger(trigger_type_));
  if (_rightCam)
    trigger_service->addTrigger(_rightCam->configureTrigger(trigger_type_));

  trigger_service->setFps(10);

  auto start = high_resolution_clock::now();

  _leftCam->start();
  _rightCam->start();

  trigger_service->start();

  while (!g_stop) {
    sleep(5);

    auto duration =
        duration_cast<milliseconds>(high_resolution_clock::now() - start);
    if (_leftCam) {
      std::cout << "Left camera captured " << _leftCam->count() << " frames; "
                << (_leftCam->count() * 1000.0 / duration.count()) << " fps"
                << std::endl;
    }

    if (_rightCam) {
      std::cout << "Right camera captured " << _rightCam->count() << " frames; "
                << (_rightCam->count() * 1000.0 / duration.count()) << " fps"
                << std::endl;
    }
  }

  if (_leftCam) {
    _leftCam->setDone();
    std::cerr << "Waiting for left camera to stop" << std::endl;
    _leftCam->join();
  }

  if (_rightCam) {
    _rightCam->setDone();
    std::cerr << "Waiting for right camera to stop" << std::endl;
    _rightCam->join();
  }

  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<milliseconds>(stop - start);

  std::cout << "Captured images for " << duration.count() / 1000.0 << " sec"
            << std::endl;

  if (_leftCam) {
    std::cout << std::endl
              << "Left camera captured " << _leftCam->count() << " frames; "
              << (_leftCam->count() * 1000.0 / duration.count()) << " fps"
              << std::endl;
  }

  if (_rightCam) {
    std::cout << "Right camera captured " << _rightCam->count() << " frames; "
              << (_rightCam->count() * 1000.0 / duration.count()) << " fps"
              << std::endl;
  }

  trigger_service->stop();
  std::cout << "Trigger fired " << trigger_service->count() << " times"
            << std::endl;

  return 0;
}
