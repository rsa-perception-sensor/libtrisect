// Simple binary for triggering cameras.   Can either
// directly trigger trigger GPIOs or call triggerd
//
// Does not configure cameras, so camera must be manually set to
// in hardware triggered mode (e.g. from running full Trisect s/w
// or run_mono_camera)
//
// Released under BSD 3-Clause license.  See LICENSE.
//
//
// Copyright (c) 2024, University of Washington
// All rights reserved.

#include <signal.h>

#include <boost/program_options.hpp>
#include <chrono>
using namespace std::chrono;

#include "libtrisect/trigger/gpio_trigger.h"
#include "libtrisect/trigger/trigger_service.h"
#include "libtrisect/trisect_cameras.h"

namespace po = boost::program_options;
using namespace libtrisect;

bool g_stop = false;

static void clear_signal_handlers() noexcept {
  signal(SIGINT, SIG_IGN);
  signal(SIGTERM, SIG_IGN);
}

static void install_signal_handlers(void (*handler)(int)) noexcept {
  signal(SIGINT, handler);
  signal(SIGTERM, handler);
}

static void signal_handler(int /*signum*/) noexcept {
  clear_signal_handlers();

  std::cerr << "Stopping triggers" << std::endl;
  g_stop = true;
}

int main(int argc, char **argv) {
  install_signal_handlers(signal_handler);

  bool useRightGpioTrigger = false;
  bool useLeftGpioTrigger = false;
  bool useTriggerd = false;

  bool isDrysect = false;

  try {
    // Parse command line options
    po::options_description options("Options");
    // clang-format off
    options.add_options()
      ("help", "produce help message")
        ("drysect", po::bool_switch(&isDrysect), "Use Drysect configuration")
        ("left-gpio", po::bool_switch(&useLeftGpioTrigger), "Use Left GPIO hardware trigger")
        ("right-gpio", po::bool_switch(&useRightGpioTrigger), "Use Right GPIO hardware trigger")
        ("triggerd", po::bool_switch(&useTriggerd), "Use triggerd to trigger cameras");
    // clang-format on

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      return 1;
    }

  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
  }

  std::shared_ptr<TriggerService> trigger_service;

  if (useTriggerd) {
    trigger_service = libtrisect::MakeTriggerService(TriggerType::TRIGGERD);
  } else {
    trigger_service = libtrisect::MakeTriggerService(TriggerType::GPIO);

    if (useLeftGpioTrigger) {
      if (isDrysect)
        trigger_service->addTrigger(std::make_shared<libtrisect::GpioTrigger>(
            DrysectCameras::LeftTriggerGpio));
      else
        trigger_service->addTrigger(std::make_shared<libtrisect::GpioTrigger>(
            TrisectCameras::LeftTriggerGpio));
    }

    if (useRightGpioTrigger) {
      if (isDrysect)
        trigger_service->addTrigger(std::make_shared<libtrisect::GpioTrigger>(
            DrysectCameras::RightTriggerGpio));
      else
        trigger_service->addTrigger(std::make_shared<libtrisect::GpioTrigger>(
            TrisectCameras::RightTriggerGpio));
    }
  }

  trigger_service->setFps(10);

  auto start = high_resolution_clock::now();
  trigger_service->start();

  while (!g_stop) {
    sleep(30);
  }

  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<milliseconds>(stop - start);

  trigger_service->stop();
  std::cout << "Trigger fired " << trigger_service->count() << " times"
            << std::endl;

  return 0;
}
