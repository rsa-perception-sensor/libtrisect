

// This tells Catch to provide a main() - only do this in one cpp file
#include <unistd.h>

#include <limits>
#include <type_traits>

#include "catch.hpp"
#include "libtrisect/trisect_camera.h"

using namespace libtrisect;

TEST_CASE("Freerunning camera", "[color camera]") {
  auto cam = ColorCamera::CreateCamera();

  if (!cam) INFO(cam.error().msg());
  REQUIRE(cam);

  cam.value()->start();

  sleep(2);

  cam.value()->setDone();
  cam.value()->join();

  std::cerr << "Captured " << cam.value()->count() << " images" << std::endl;
}
