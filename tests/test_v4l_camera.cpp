

// This tells catch to provide a main() - only do this in one cpp file
#include <unistd.h>

#include <limits>
#include <type_traits>

#include "catch.hpp"
#include "libtrisect/trigger/gpio_trigger.h"
#include "libtrisect/trigger/trigger_loop.h"
#include "libtrisect/trisect_cameras.h"

using namespace libtrisect;

TEST_CASE("Freerunning camera", "[camera]") {
  auto cam = V4LCamera::CreateCamera("/dev/video0");

  if (!cam) INFO(cam.error().msg());
  REQUIRE(cam);

  std::cerr << "Camera trigger mode " << cam.value()->getTriggerSource()
            << std::endl;
  std::cout << "Camera image size   " << cam.value()->width() << " x "
            << cam.value()->height() << std::endl;

  cam.value()->disableHardwareTrigger();

  cam.value()->start();

  sleep(2);

  cam.value()->setDone();
  cam.value()->join();

  std::cerr << "Captured " << cam.value()->count() << " images" << std::endl;
}

TEST_CASE("Right triggered camera", "[camera]") {
  // Must use RightCamera (or MonoCamera) otherwise enableTrigger won't do
  // anything
  auto cam_factory = TrisectCameras();
  auto cam = cam_factory.RightCamera(MonoCamera::FORMAT_GREY8);

  if (!cam) INFO(cam.error().msg());
  REQUIRE(cam);

  cam.value()->enableHardwareTrigger();

  std::cerr << "Camera trigger mode " << cam.value()->getTriggerSource()
            << std::endl;
  std::cout << "Camera image size   " << cam.value()->width() << " x "
            << cam.value()->height() << std::endl;

  TriggerLoop loop(100);

  // loop.addTrigger( std::shared_ptr<Trigger>( new GpioTrigger(
  // "/dev/gpiochip0", 124 ) ) ); loop.addTrigger( RightCamera::getTrigger() );
  loop.addTrigger(std::shared_ptr<TriggerAction>(
      new GpioTrigger(TrisectCameras::LeftTriggerGpio)));
  loop.addTrigger(std::shared_ptr<TriggerAction>(
      new GpioTrigger(TrisectCameras::RightTriggerGpio)));

  cam.value()->start();
  loop.start();

  sleep(2);

  cam.value()->setDone();
  cam.value()->join();

  cam.value()->disableHardwareTrigger();

  loop.setDone();
  loop.join();

  std::cerr << "Captured " << cam.value()->count() << " images" << std::endl;
  CHECK(loop.count() == Approx(20).margin(2));
  CHECK(cam.value()->count() == Approx(20).margin(2));
}

TEST_CASE("Both triggered cameras", "[camera]") {
  auto cam_factory = TrisectCameras();
  auto left_result = cam_factory.LeftCamera(MonoCamera::FORMAT_GREY8);
  auto right_result = cam_factory.RightCamera(MonoCamera::FORMAT_GREY8);

  if (!left_result) INFO(left_result.error().msg());
  REQUIRE(left_result);

  if (!right_result) INFO(right_result.error().msg());
  REQUIRE(right_result);

  auto left = left_result.value();
  auto right = right_result.value();

  // std::cerr << "Camera trigger mode " << cam.value()->getTriggerSource() <<
  // std::endl; std::cout << "Camera image size   " << cam.value()->width() << "
  // x " << cam.value()->height() << std::endl;

  TriggerLoop loop;

  loop.addTrigger(left->configureTrigger(TriggerType::GPIO));
  loop.addTrigger(right->configureTrigger(TriggerType::GPIO));
  loop.setFps(10);

  left->start();
  right->start();
  loop.start();

  sleep(2);

  left->setDone();
  right->setDone();

  left->join();
  right->join();

  left->disableHardwareTrigger();
  right->disableHardwareTrigger();

  loop.setDone();
  loop.join();

  std::cerr << "Captured " << left->count() << " images on left; "
            << right->count() << " images on right" << std::endl;
  CHECK(loop.count() == Approx(20).margin(2));
  CHECK(left->count() == Approx(20).margin(2));
  CHECK(right->count() == Approx(20).margin(2));
}
