
// This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_MAIN
#include <limits>
#include <type_traits>

#include "catch.hpp"
#include "libtrisect/trigger/trigger_action.h"
#include "libtrisect/trigger/trigger_loop.h"

using namespace libtrisect;

// This tests the output of the `get_nth_prime` function
TEST_CASE("Constructor", "[triggerloop]") {
  TriggerLoop loop(100);

  CHECK(1 == 1);
}

// This tests the output of the `get_nth_prime` function
TEST_CASE("RUN", "[triggerloop]") {
  TriggerLoop loop(100);

  loop.start();

  sleep(2);

  loop.setDone();
  loop.join();

  CHECK(loop.count() == Approx(20).margin(2));
}

TEST_CASE("One trigger", "[triggerloop]") {
  TriggerLoop loop(100);

  auto trigger =
      std::shared_ptr<TriggerAction>(new FakeTrigger("Fake Trigger #1"));
  loop.addTrigger(trigger);

  loop.start();

  sleep(2);

  loop.setDone();
  loop.join();

  CHECK(loop.count() == Approx(20).margin(2));
  CHECK(trigger->count() == Approx(20).margin(2));
}

TEST_CASE("Multiple triggers", "[triggerloop]") {
  TriggerLoop loop(100);

  auto one = std::shared_ptr<TriggerAction>(new FakeTrigger("Fake Trigger #1"));
  loop.addTrigger(one);

  auto two = std::shared_ptr<TriggerAction>(new FakeTrigger("Fake Trigger #2"));
  loop.addTrigger(two);

  auto three =
      std::shared_ptr<TriggerAction>(new FakeTrigger("Fake Trigger #3"));
  loop.addTrigger(three);

  loop.start();

  sleep(2);

  loop.setDone();
  loop.join();

  CHECK(loop.count() == Approx(20).margin(2));
  CHECK(one->count() == Approx(20).margin(2));
  CHECK(two->count() == Approx(20).margin(2));
  CHECK(three->count() == Approx(20).margin(2));
}
