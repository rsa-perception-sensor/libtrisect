
// This tells Catch to provide a main() - only do this in one cpp file
#include <limits>
#include <type_traits>

#include "catch.hpp"
#include "libtrisect/trigger/gpio_trigger.h"

using namespace libtrisect;

TEST_CASE("GpioTrigger Constructor", "[gpiotrigger]") {
  GpioTrigger(GpioConfig({"/dev/gpiochip0", 124}));
}
