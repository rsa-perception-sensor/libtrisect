/*
 *  V4L2 video capture example
 *
 *  This program can be used and distributed without restrictions.
 *
 *      This program is provided with the V4L2 API
 * see http://linuxtv.org/docs.php for more information
 */

//[    0.770098] gpiochip_setup_dev: registered GPIOs 288 to 511 on device:
// gpiochip0 (tegra-gpio) [    0.778618] gpiochip_setup_dev: registered GPIOs
// 248 to 287 on device: gpiochip1 (tegra-gpio-aon)
//
//  VSYNC_CAM2_1   on pin CAM0_PWDN, SOM pin 114, GPIO3_PP.04, == (15 * 8)+4 ==
//  124
//                           on main GPIO (gpiochip0) with offset 288 == 412?
//  VSYNC_CAM2_2   on GPIO06,        SOM pin 130, GPIO3_PCC.03 == (2*8) + 3 ==
//  19 on AON
//                           on AON gpio (gpiochip1)  with offset 248 == 267?
//
//

#include <assert.h>
#include <errno.h>
#include <fcntl.h>  /* low-level i/o */
#include <getopt.h> /* getopt_long() */
#include <libcsi_ioctl.h>
#include <linux/gpio.h>
#include <linux/videodev2.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

const int TRIGGER_SOURCE = V4L2_TRIGGER_SOURCE_LINE0;
const int TRIGGER_EDGE = V4L2_TRIGGER_ACTIVATION_FALLING_EDGE;

char *dev_name, *gpio_name;
int video_fd = -1, gpio_fd = -1;
int vsync_pin = -1;

// For saving state on program start
int original_trigger;

static void errno_exit(const char *s) {
  fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
  exit(EXIT_FAILURE);
}

static int xioctl(int fh, int request, void *arg) {
  int r;

  do {
    r = ioctl(fh, request, arg);
  } while (-1 == r && EINTR == errno);

  return r;
}

void set_trigger_mode(bool enable) {
  int ctl = (enable ? VIDIOC_TRIGGER_MODE_ON : VIDIOC_TRIGGER_MODE_OFF);
  if (-1 == xioctl(video_fd, ctl, &ctl)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is not a V4L2 device\n", dev_name);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_S_TRIGGER_MODE_{ON|OFF}");
    }
  }

  if (!enable) {
    if (-1 == xioctl(video_fd, ctl, &ctl)) {
      if (EINVAL == errno) {
        fprintf(stderr, "%s is not a V4L2 device\n", dev_name);
        exit(EXIT_FAILURE);
      } else {
        errno_exit("VIDIOC_S_TRIGGER_MODE_{ON|OFF}");
      }
    }
  }
}

void set_trigger_source(int trigger) {
  if (-1 == xioctl(video_fd, VIDIOC_S_TRIGGER_SOURCE, &trigger)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is not a V4L2 device\n", dev_name);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_S_TRIGGER_SOURCE");
    }
  }
}

int get_trigger_source(void) {
  int trigger = -1;
  if (-1 == xioctl(video_fd, VIDIOC_G_TRIGGER_SOURCE, &trigger)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is not V4L2 device\n", dev_name);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_G_TRIGGER_SOURCE");
    }
  }

  return trigger;
}

void set_trigger_activation(int mode) {
  if (-1 == xioctl(video_fd, VIDIOC_S_TRIGGER_ACTIVATION, &mode)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is not a V4L2 device\n", dev_name);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_S_TRIGGER_ACTIVATION");
    }
  }
}

int get_trigger_activation(void) {
  int mode = -1;
  if (-1 == xioctl(video_fd, VIDIOC_G_TRIGGER_ACTIVATION, &mode)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is not V4L2 device\n", dev_name);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_G_TRIGGER_SOURCE");
    }
  }

  return mode;
}

static void configure_device(void) {
  struct v4l2_capability cap;
  struct v4l2_cropcap cropcap;
  struct v4l2_crop crop;
  struct v4l2_format fmt;
  unsigned int min;

  if (-1 == xioctl(video_fd, VIDIOC_QUERYCAP, &cap)) {
    if (EINVAL == errno) {
      fprintf(stderr, "%s is not a  V4L2 device\n", dev_name);
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_QUERYCAP");
    }
  }

  if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
    fprintf(stderr, "%s is not a video capture device\n", dev_name);
    exit(EXIT_FAILURE);
  }

  original_trigger = get_trigger_source();
  fprintf(stderr, "Original trigger source %d\n", original_trigger);

  set_trigger_source(TRIGGER_SOURCE);
  set_trigger_activation(TRIGGER_EDGE);

  set_trigger_mode(true);

  int trigger = get_trigger_source();
  fprintf(stderr, "Now set to trigger source %d\n", trigger);

  // CLEAR(cropcap);

  // cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  // if (0 == xioctl(video_fd, VIDIOC_CROPCAP, &cropcap)) {
  //         crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  //         crop.c = cropcap.defrect; /* reset to default */

  //         if (-1 == xioctl(video_fd, VIDIOC_S_CROP, &crop)) {
  //                 switch (errno) {
  //                 case EINVAL:
  //                         /* Cropping not supported. */
  //                         break;
  //                 default:
  //                         /* Errors ignored. */
  //                         break;
  //                 }
  //         }
  // } else {
  //         /* Errors ignored. */
  // }

  // CLEAR(fmt);

  // fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  // if (force_format) {
  // fprintf(stderr, "Set H264\r\n");
  //         fmt.fmt.pix.width       = 640; //replace
  //         fmt.fmt.pix.height      = 480; //replace
  //         fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_H264; //replace
  //         fmt.fmt.pix.field       = V4L2_FIELD_ANY;

  //         if (-1 == xioctl(video_fd, VIDIOC_S_FMT, &fmt))
  //                 errno_exit("VIDIOC_S_FMT");

  //         /* Note VIDIOC_S_FMT may change width and height. */
  // } else {
  //         /* Preserve original settings as set by v4l2-ctl for example */
  //         if (-1 == xioctl(video_fd, VIDIOC_G_FMT, &fmt))
  //                 errno_exit("VIDIOC_G_FMT");
  // }
}

static void unconfigure_device(void) {
  set_trigger_mode(true);

  // Restore original trigger settings
  fprintf(stderr, "Restoring original trigger source %d\n", original_trigger);
  set_trigger_source(original_trigger);
  int trigger = get_trigger_source();
  fprintf(stderr, "Trigger source now %d\n", trigger);
}

static void close_device(void) {
  if (-1 == close(video_fd)) errno_exit("close");

  video_fd = -1;
}

static void close_gpio(void) {
  if (-1 == close(gpio_fd)) errno_exit("close");

  gpio_fd = -1;
}

static void open_device(void) {
  struct stat st;

  if (-1 == stat(dev_name, &st)) {
    fprintf(stderr, "Cannot identify '%s': %d, %s\n", dev_name, errno,
            strerror(errno));
    exit(EXIT_FAILURE);
  }

  if (!S_ISCHR(st.st_mode)) {
    fprintf(stderr, "%s is not a char device\n", dev_name);
    exit(EXIT_FAILURE);
  }

  video_fd = open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

  if (-1 == video_fd) {
    fprintf(stderr, "Cannot open '%s': %d, %s\n", dev_name, errno,
            strerror(errno));
    exit(EXIT_FAILURE);
  }
}

static void open_gpio(void) {
  struct stat st;

  if (-1 == stat(gpio_name, &st)) {
    fprintf(stderr, "Cannot identify '%s': %d, %s\n", gpio_name, errno,
            strerror(errno));
    exit(EXIT_FAILURE);
  }

  if (!S_ISCHR(st.st_mode)) {
    fprintf(stderr, "%s is not a char device\n", dev_name);
    exit(EXIT_FAILURE);
  }

  gpio_fd = open(gpio_name, O_RDWR /* required */ | O_NONBLOCK, 0);

  if (-1 == gpio_fd) {
    fprintf(stderr, "Cannot open '%s': %d, %s\n", gpio_name, errno,
            strerror(errno));
    exit(EXIT_FAILURE);
  }

  struct gpiochip_info info;
  struct gpioline_info line_info;
  int ret;

  // Query GPIO chip information
  ret = ioctl(gpio_fd, GPIO_GET_CHIPINFO_IOCTL, &info);
  if (ret == -1) {
    printf("Unable to get chip info from ioctl: %s", strerror(errno));
    return;
  }
  fprintf(stdout, "Chip name: %s\n", info.name);
  fprintf(stdout, "Chip label: %s\n", info.label);
  fprintf(stdout, "Number of lines: %d\n", info.lines);

  line_info.line_offset = vsync_pin;
  ret = ioctl(gpio_fd, GPIO_GET_LINEINFO_IOCTL, &line_info);
  if (ret == -1) {
    printf("Unable to get line info from offset %d: %s", vsync_pin,
           strerror(errno));
  } else {
    printf(
        "offset: %d, name: %s, consumer: %s. "
        "Flags:\t[%s]\t[%s]\t[%s]\t[%s]\t[%s]\n",
        vsync_pin, line_info.name, line_info.consumer,
        (line_info.flags & GPIOLINE_FLAG_IS_OUT) ? "OUTPUT" : "INPUT",
        (line_info.flags & GPIOLINE_FLAG_ACTIVE_LOW) ? "ACTIVE_LOW"
                                                     : "ACTIVE_HIGHT",
        (line_info.flags & GPIOLINE_FLAG_OPEN_DRAIN) ? "OPEN_DRAIN" : "...",
        (line_info.flags & GPIOLINE_FLAG_OPEN_SOURCE) ? "OPENSOURCE" : "...",
        (line_info.flags & GPIOLINE_FLAG_KERNEL) ? "KERNEL" : "");
  }
}

static void usage(FILE *fp, int argc, char **argv) {
  fprintf(fp,
          "Usage: %s [options]\n\n"
          "Version 1.3\n"
          "Options:\n"
          "-d | --device name   Video device name [default: %s]\n"
          "-h | --help          Print this message\n"
          "",
          argv[0], dev_name);
}

static const char short_options[] = "d:hmruofc:";

static const struct option long_options[] = {
    {"device", required_argument, NULL, 'd'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}};

int main(int argc, char **argv) {
  //     dev_name = "/dev/video0";
  //     gpio_name = "/dev/gpiochip1";
  //     vsync_pin = 19;

  dev_name = "/dev/video1";
  gpio_name = "/dev/gpiochip0";
  vsync_pin = 124;

  for (;;) {
    int idx;
    int c;

    c = getopt_long(argc, argv, short_options, long_options, &idx);

    if (-1 == c) break;

    switch (c) {
      case 0: /* getopt_long() flag */
        break;

      case 'd':
        dev_name = optarg;
        break;

      case 'h':
        usage(stdout, argc, argv);
        exit(EXIT_SUCCESS);

      default:
        usage(stderr, argc, argv);
        exit(EXIT_FAILURE);
    }
  }

  open_device();
  configure_device();
  close_device();

  open_gpio();

  struct gpiohandle_request rq;
  rq.lineoffsets[0] = vsync_pin;
  rq.lines = 1;
  rq.flags = GPIOHANDLE_REQUEST_OUTPUT;

  int ret = ioctl(gpio_fd, GPIO_GET_LINEHANDLE_IOCTL, &rq);
  if (ret == -1) {
    printf("Unable to line handle from ioctl : %s", strerror(errno));
  }

  struct gpiohandle_data data;
  data.values[0] = 0;

  for (int i = 0; i < 0; i++) {
    data.values[0] = ~data.values[0];
    fprintf(stdout, "Set: %d\n", data.values[0]);
    ret = ioctl(rq.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);
    usleep(100000);

    data.values[0] = ~data.values[0];
    fprintf(stdout, "Set: %d\n", data.values[0]);
    ret = ioctl(rq.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);
    usleep(100000);
  }

  close_gpio();

  open_device();
  unconfigure_device();
  close_device();
  fprintf(stderr, "\n");
  return 0;
}
